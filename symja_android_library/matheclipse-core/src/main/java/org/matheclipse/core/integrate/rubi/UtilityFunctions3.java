package org.matheclipse.core.integrate.rubi;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * UtilityFunctions rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class UtilityFunctions3 { 
  public static IAST RULES = List( 
ISetDelayed(SimpProduct(Power(u_,m_DEFAULT),Power(v_,n_DEFAULT)),
    Condition(Module(List(Set($s("gcd"),GCD(m,n)),w),CompoundExpression(Set(w,Expand(Times(Power(u,Times(m,Power($s("gcd"),CN1))),Power(v,Times(n,Power($s("gcd"),CN1)))))),Condition(SimpAux(Power(w,$s("gcd"))),Or(SqrtNumberQ(w),SqrtNumberSumQ(w))))),And(And(And(And(And(And(RationalQ(List(m,n)),Greater(m,C0)),Greater(n,C0)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),PositiveQ(u)),PositiveQ(v)))),
ISetDelayed(SimpProduct(Power(u_,m_),Power(v_,n_DEFAULT)),
    Condition(Module(List(Set($s("gcd"),GCD(m,n)),w),CompoundExpression(Set(w,Simp(Times(Power(u,Times(m,Power($s("gcd"),CN1))),Power(v,Times(n,Power($s("gcd"),CN1)))))),Condition(SimpAux(Power(w,$s("gcd"))),Or(SqrtNumberQ(w),SqrtNumberSumQ(w))))),And(And(And(And(And(And(RationalQ(List(m,n)),Less(m,C0)),Greater(n,C0)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),PositiveQ(u)),PositiveQ(v)))),
ISetDelayed(SimpAux(Power(u_,n_)),
    Condition(Times(Power(CN1,n),SimpAux(Power(Map($s("Minus"),u),n))),And(And(SumQ(u),IntIntegerQ(n)),Less(NumericFactor(Part(u,C1)),C0)))),
ISetDelayed(SimpProduct(u_,v_),
    Condition(Times(CN1,SimpAux(Times(Map($s("Minus"),u),v))),And(And(SumQ(u),Less(NumericFactor(Part(u,C1)),C0)),Not(And(SqrtNumberSumQ(u),SqrtNumberSumQ(v)))))),
ISetDelayed(SimpAux(Power(u_,n_)),
    Condition(Module(List(Set($s("lst"),CommonFactors(Apply($s("List"),u)))),Condition(Times(Simp(Power(Part($s("lst"),C1),n)),SimpAux(Power(Apply(Plus,Rest($s("lst"))),n))),UnsameQ(Part($s("lst"),C1),C1))),And(SumQ(u),IntIntegerQ(n)))),
ISetDelayed(SimpAux(Power(u_,n_)),
    Condition(Module(List(Set($s("lst"),CommonNumericFactors(Apply($s("List"),u)))),Condition(Times(Power(Part($s("lst"),C1),n),SimpAux(Power(Apply(Plus,Rest($s("lst"))),n))),UnsameQ(Part($s("lst"),C1),C1))),And(And(SumQ(u),Not(IntIntegerQ(n))),Not(SqrtNumberSumQ(u))))),
ISetDelayed(SimpProduct(Power(Plus(a_,b_),m_),Power(Plus(c_,$p(d)),n_DEFAULT)),
    Condition(Simp(Times(Power(Plus(a,b),Plus(m,n)),Power(Times(d,Power(b,CN1)),n))),And(And(IntIntegerQ(n),ZeroQ(Plus(Times(a,d),Times(CN1,b,c)))),Not(SqrtNumberSumQ(Plus(a,b)))))),
ISetDelayed(SimpProduct(Power(u_,m_DEFAULT),Power(v_,n_DEFAULT)),
    Condition(Times(Power(CN1,n),Power(u,Plus(m,n))),And(And(And(IntIntegerQ(n),ZeroQ(Plus(u,v))),Not(RationalQ(u))),Or(Not(IntIntegerQ(m)),LessEqual(SmartLeafCount(u),SmartLeafCount(v)))))),
ISetDelayed(SimpProduct(Power(Plus(a_,b_),n_DEFAULT),Power(Plus(c_,$p(d)),n_DEFAULT)),
    Condition(Simp(Power(Plus(Power(a,C2),Times(CN1,Power(b,C2))),n)),And(And(ZeroQ(Plus(a,Times(CN1,c))),ZeroQ(Plus(b,d))),IntIntegerQ(n)))),
ISetDelayed(SimpSum(Times(Power(u_,CN1),a_),Times(Power(u_,CN1),b_)),
    Condition(C1,SameQ(Plus(a,b),u))),
ISetDelayed(SimpSum(a_,Times(Power(Plus(c_,$p(d)),C2),b_DEFAULT)),
    Condition(Simp(Times(b,d,Plus(Times(C2,c),d))),ZeroQ(Plus(a,Times(b,Power(c,C2)))))),
ISetDelayed(SimpSum(a_,Times(Plus(c_DEFAULT,$p(d)),Power(Plus($p(e),f_),CN1),b_DEFAULT)),
    Condition(SimpAux(Times(ContentFactor(Plus(Times(a,e),Times(b,c))),Power(Plus(e,f),CN1))),And(And(And(And(And(And(NonsumQ(a),NonsumQ(b)),NonsumQ(c)),NonsumQ(d)),NonsumQ(e)),NonsumQ(f)),ZeroQ(Plus(Times(a,f),Times(b,d)))))),
ISetDelayed(SimpProduct(Power(v_,m_DEFAULT),Power(Plus(Times(Power(v_,n_),b_DEFAULT),a_),p_DEFAULT)),
    Condition(SimpAux(Times(Power(v,Plus(m,Times(n,p))),Simp(Power(Plus(Times(a,Power(v,Times(CN1,n))),b),p)))),And(And(And(IntIntegerQ(p),RationalQ(List(m,n))),Less(n,C0)),Not(SqrtNumberSumQ(Plus(a,Times(b,Power(v,n)))))))),
ISetDelayed(SimpProduct(Power(c_,m_DEFAULT),Power(Plus(Times(Power(c_,p_DEFAULT),a_DEFAULT),Times(Power(c_,q_DEFAULT),b_DEFAULT)),n_DEFAULT)),
    Condition(SimpAux(Power(Plus(Times(a,Power(c,Plus(p,Times(m,Power(n,CN1))))),Times(b,Power(c,Plus(q,Times(m,Power(n,CN1)))))),n)),And(IntIntegerQ(n),RationalQ(List(m,p,q))))),
ISetDelayed(SimpSum(Times(Power(n_,CN1D2),u_DEFAULT),Times(Power(n_,C1D2),v_DEFAULT)),
    Condition(Times(Plus(C1,Times(n,NumericFactor(v),Power(NumericFactor(u),CN1))),u,Power(Sqrt(n),CN1)),And(RationalQ(n),SameQ(NonnumericFactors(u),NonnumericFactors(v))))),
ISetDelayed(SimpSum(u_,Times(Plus(a_,b_),v_)),
    Condition(Module(List(Set($s("tmp"),SimpAux(Times(v,a)))),Condition(SimpAux(Plus(Times(Plus(C1,Times(NumericFactor($s("tmp")),Power(NumericFactor(u),CN1))),u),SimpAux(Times(v,b)))),SameQ(NonnumericFactors($s("tmp")),NonnumericFactors(u)))),And(And(NonsumQ(a),NonsumQ(u)),NonsumQ(v)))),
ISetDelayed(SimpSum(Times(Plus(a_,b_),u_DEFAULT),Times(Plus(c_,$p(d)),v_)),
    Condition(Module(List(Set($s("tmp1"),SimpAux(Times(v,c))),Set($s("tmp2"),SimpAux(Times(u,a)))),Condition(CompoundExpression(Set($s("tmp1"),Times(NumericFactor($s("tmp1")),Power(NumericFactor($s("tmp2")),CN1))),If(Or(IntIntegerQ($s("tmp1")),Less(Less(CN2,$s("tmp1")),C0)),SimpAux(Plus(Times(u,Plus(Times(Plus(C1,$s("tmp1")),a),b)),SimpAux(Times(v,d)))),SimpAux(Plus(SimpAux(Times(u,b)),Times(v,Plus(Times(Plus(C1,Power($s("tmp1"),CN1)),c),d)))))),SameQ(NonnumericFactors($s("tmp1")),NonnumericFactors($s("tmp2"))))),And(And(And(NonsumQ(a),NonsumQ(c)),NonsumQ(u)),NonsumQ(v)))),
ISetDelayed(SimpAux(Power(E,Times(Plus(Times(Log(v_),a_DEFAULT),b_),c_DEFAULT))),
    Times(SimpAux(Power(v,Times(a,c))),SimpAux(Power(E,Times(b,c))))),
ISetDelayed(SimpAux(Power(E,Times(ArcTanh(v_),n_))),
    Condition(Simp(Power(Plus(CN1,Times(C2,Power(Plus(C1,Times(CN1,v)),CN1))),Times(Rational(C1,C2),n))),EvenQ(n))),
ISetDelayed(SimpProduct(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(C1,w_),m_)),
    Condition(Simp(Times(Power(Plus(C1,Times(CN1,v)),Plus(m,Times(CN1,Rational(C1,C2),n))),Power(Plus(C1,v),Plus(m,Times(Rational(C1,C2),n))))),And(And(OddQ(n),HalfIntegerQ(m)),ZeroQ(Plus(Power(v,C2),w))))),
ISetDelayed(SimpAux(Power(E,Times(ArcCoth(v_),n_))),
    Condition(Simp(Power(Plus(C1,Times(CN1,C2,Power(Plus(C1,Times(CN1,v)),CN1))),Times(Rational(C1,C2),n))),EvenQ(n))),
ISetDelayed(SimpProduct(Power(E,Times(ArcCoth(v_),n_DEFAULT)),Power(Plus(C1,w_),m_)),
    Condition(Simp(Times(Power(Plus(CN1,v),Plus(m,Times(CN1,Rational(C1,C2),n))),Power(Plus(C1,v),Plus(m,Times(Rational(C1,C2),n))),Power(Power(v,Times(C2,m)),CN1))),And(And(OddQ(n),HalfIntegerQ(m)),ZeroQ(Plus(Power(v,C2),Power(w,CN1)))))),
ISetDelayed(SimpAux(Power(E,Times(ProductLog(v_),n_DEFAULT))),
    Condition(Simp(Times(Power(v,n),Power(Power(ProductLog(v),n),CN1))),And(IntIntegerQ(n),Greater(n,C0)))),
ISetDelayed(SimpSum(Times(Power(Cos(z_),C2),u_DEFAULT),Times(Power(Sin(z_),C2),v_DEFAULT)),
    Condition(u,SameQ(u,v))),
ISetDelayed(SimpSum(Times(Power(Sec(z_),C2),u_DEFAULT),Times(Power(Tan(z_),C2),v_DEFAULT)),
    Condition(u,SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(Times(Power(Csc(z_),C2),u_DEFAULT),Times(Power(Cot(z_),C2),v_DEFAULT)),
    Condition(u,SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Sin(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Cos(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Cos(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Sin(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Tan(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Sec(z),C2)),SameQ(u,v))),
ISetDelayed(SimpSum(u_,Times(Power(Cot(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Csc(z),C2)),SameQ(u,v))),
ISetDelayed(SimpSum(u_,Times(Power(Sec(z_),C2),v_DEFAULT)),
    Condition(Times(v,Power(Tan(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Csc(z_),C2),v_DEFAULT)),
    Condition(Times(v,Power(Cot(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpAux(Power(Plus(Times(Cos(v_),a_DEFAULT),Times(Sin(v_),b_DEFAULT)),n_)),
    Condition(SimpAux(Power(Plus(Times(Cos(v),Power(a,CN1)),Times(Sin(v),Power(b,CN1))),Times(CN1,n))),And(And(IntIntegerQ(n),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
ISetDelayed(SimpSum(Times(Power(Cosh(z_),C2),u_DEFAULT),Times(Power(Sinh(z_),C2),v_DEFAULT)),
    Condition(u,SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(Times(Power(Sech(z_),C2),u_DEFAULT),Times(Power(Tanh(z_),C2),v_DEFAULT)),
    Condition(u,SameQ(u,v))),
ISetDelayed(SimpSum(Times(Power(Coth(z_),C2),u_DEFAULT),Times(Power(Csch(z_),C2),v_DEFAULT)),
    Condition(u,SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Sinh(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Cosh(z),C2)),SameQ(u,v))),
ISetDelayed(SimpSum(u_,Times(Power(Cosh(z_),C2),v_DEFAULT)),
    Condition(Times(v,Power(Sinh(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Tanh(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Sech(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Coth(z_),C2),v_DEFAULT)),
    Condition(Times(v,Power(Csch(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Sech(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Tanh(z),C2)),SameQ(u,Times(CN1,v)))),
ISetDelayed(SimpSum(u_,Times(Power(Csch(z_),C2),v_DEFAULT)),
    Condition(Times(u,Power(Coth(z),C2)),SameQ(u,v))),
ISetDelayed(SimpAux(Power(Plus(Times(Cosh(v_),a_DEFAULT),Times(Sinh(v_),b_DEFAULT)),n_)),
    Condition(SimpAux(Power(Plus(Times(Cosh(v),Power(a,CN1)),Times(CN1,Sinh(v),Power(b,CN1))),Times(CN1,n))),And(And(IntIntegerQ(n),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(SimpSum(Times(Power($(f_,a_),n_DEFAULT),u_DEFAULT),Times(Power($(f_,a_),n_DEFAULT),v_DEFAULT)),
    Condition(SimpAux(Times(Simp(Simplify(Plus(u,v))),Power($(f,a),n))),MemberQ(List($s("Erf"),$s("Erfc"),$s("Erfi"),$s("FresnelS"),$s("FresnelC"),$s("ExpIntegralEi"),$s("SinIntegral"),$s("CosIntegral"),$s("SinhIntegral"),$s("CoshIntegral"),$s("LogIntegral")),f))),
ISetDelayed(SimpSum(Times(Power($(f_,a_,b_),n_DEFAULT),u_DEFAULT),Times(Power($(f_,a_,b_),n_DEFAULT),v_DEFAULT)),
    Condition(SimpAux(Times(Simp(Simplify(Plus(u,v))),Power($(f,a,b),n))),MemberQ(List($s("Int"),$s("Gamma"),$s("PolyLog"),$s("EllipticF"),$s("EllipticE")),f))),
ISetDelayed(ExpandIntegrandQ(m_,n_,p_),
    And(And(And(IntIntegerQ(p),Greater(p,C0)),NonzeroQ(Plus(m,Times(CN1,n),C1))),If(ZeroQ(Plus(n,Times(CN1,C1))),Or(Or(Not(IntIntegerQ(m)),And(Less(m,C0),Not(LessEqual(LessEqual(Plus(m,p,C2),C0),Plus(m,Times(C2,p),C2))))),LessEqual(p,Plus(m,C2))),Or(Or(Equal(p,C2),Not(IntIntegerQ(Times(Plus(m,C1),Power(n,CN1))))),And(Not(And(Less(C0,Times(Plus(m,C1),Power(n,CN1))),LessEqual(Times(Plus(m,C1),Power(n,CN1)),C3))),Not(LessEqual(Times(Plus(m,C1),Power(n,CN1)),Times(CN1,Plus(p,C1))))))))),
ISetDelayed(ExpnExpand(u_,x_Symbol),
    ExpnExpandAux(ExpandExpression(u,x),x)),
ISetDelayed(ExpnExpandAux(Plus(Times(Power(x_,CN1),$p(e,true)),Times(Power(Plus(Times($p(d,true),x_),c_),CN1),f_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Plus(ExpnExpandAux(u,x),Times(c,e,Power(Times(x,Plus(c,Times(d,x))),CN1))),And(FreeQ(List(c,d,e,f),x),ZeroQ(Plus(Times(d,e),f))))),
ISetDelayed(ExpnExpandAux(Plus(Times(Power(Plus(Times(b_DEFAULT,x_),a_),CN1),$p(e,true)),Times(Power(Plus(Times($p(d,true),x_),c_),CN1),f_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Plus(ExpnExpandAux(u,x),Times(Plus(Times(c,e),Times(a,f)),Power(Plus(Times(a,c),Times(b,d,Power(x,C2))),CN1))),And(And(FreeQ(List(a,b,c,d,e,f),x),ZeroQ(Plus(Times(d,e),Times(b,f)))),ZeroQ(Plus(Times(b,c),Times(a,d)))))),
ISetDelayed(ExpnExpandAux(u_,x_Symbol),
    u),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(x_,p_DEFAULT),a_DEFAULT),Times(Power(x_,q_DEFAULT),b_DEFAULT)),n_),u_DEFAULT),x_Symbol),
    Condition(ExpandExpression(Times(u,Power(x,Times(n,p)),Power(Plus(a,Times(b,Power(x,Plus(q,Times(CN1,p))))),n)),x),And(And(FreeQ(List(a,b),x),IntIntegerQ(List(n,p,q))),GreaterEqual(Plus(q,Times(CN1,p)),C0)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(c_,m_),$p(d,true)),Times(b_DEFAULT,v_),a_DEFAULT),p_),u_DEFAULT),x_Symbol),
    Condition(Module(List($s("tmp")),ReplaceAll(ExpandExpression(Times(u,Power(Plus(a,Times(d,$s("tmp")),Times(b,v)),p)),x),List(Rule($s("tmp"),Power(c,m))))),And(And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(p)),Less(p,C0)),FractionQ(m)),Not(FreeQ(v,x))))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(c_,m_),b_DEFAULT,v_),a_DEFAULT),p_),u_DEFAULT),x_Symbol),
    Condition(Module(List($s("tmp")),ReplaceAll(ExpandExpression(Times(u,Power(Plus(a,Times(b,$s("tmp"),v)),p)),x),List(Rule($s("tmp"),Power(c,m))))),And(And(And(And(FreeQ(List(a,b,c),x),IntIntegerQ(p)),Less(p,C0)),FractionQ(m)),Not(FreeQ(v,x))))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(x_,n_),b_DEFAULT),a_),p_),Power(Plus(Times(Power(x_,m_DEFAULT),$p(d,true)),c_DEFAULT),q_DEFAULT)),x_Symbol),
    Condition(Module(List($s("aa"),$s("bb")),RegularizeTerm(ReplaceAll(Apart(Times(Power(Plus(c,Times(d,Power(x,m))),q),Power(Plus($s("aa"),Times($s("bb"),Power(x,n))),p)),x),List(Rule($s("aa"),a),Rule($s("bb"),b))),x)),And(And(And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n,p,q))),Less(p,C0)),Greater(n,C1)),Or(GreaterEqual(m,n),Less(m,C0))),Greater(q,C0)))),
ISetDelayed(ExpandExpression(Times(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_),Power(Plus(Times(Power(x_,n_DEFAULT),$p(d,true)),c_),CN1),Power(x_,m_)),x_Symbol),
    Condition(Plus(Times(a,Power(x,m),Power(c,CN1)),Dist(Times(Plus(Times(b,c),Times(CN1,a,d)),Power(c,CN1)),ExpandExpression(Times(Power(x,Plus(m,n)),Power(Plus(c,Times(d,Power(x,n))),CN1)),x))),And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n))),Greater(n,C0)),Less(m,C0)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(x_,k_DEFAULT),b_DEFAULT),Times(Power(x_,j_),c_DEFAULT),a_),n_),u_DEFAULT),x_Symbol),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Rational(C1,C2),b),Times(c,Power(x,k))),Times(C2,n)),Power(Power(c,n),CN1)),x),And(And(And(And(FreeQ(List(a,b,c,j,k),x),IntIntegerQ(List(n,k,j))),Equal(j,Times(C2,k))),Less(n,C0)),ZeroQ(Plus(Power(b,C2),Times(CN1,C4,a,c)))))),
ISetDelayed(ExpandExpression(Power(u_,n_),x_Symbol),
    Condition(Power(Plus(Coefficient(u,x,C0),Times(Coefficient(u,x,C1),x),Times(Coefficient(u,x,C2),Power(x,C2))),n),And(And(And(And(RationalQ(n),Less(n,C0)),IntPolynomialQ(u,x)),Equal(Exponent(u,x),C2)),Not(MatchQ(u,Condition(Plus(a_DEFAULT,Times(b_DEFAULT,x),Times(c_DEFAULT,Power(x,C2))),FreeQ(List(a,b,c),x))))))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(x_,C4),b_DEFAULT),a_),CN1),u_),x_Symbol),
    Condition(Plus(Times(Plus(Coefficient(u,x,C0),Times(Coefficient(u,x,C2),Power(x,C2))),Power(Plus(a,Times(b,Power(x,C4))),CN1)),Times(Coefficient(u,x,C1),x,Power(Plus(a,Times(b,Power(x,C4))),CN1)),Times(Coefficient(u,x,C3),Power(x,C3),Power(Plus(a,Times(b,Power(x,C4))),CN1))),And(And(FreeQ(List(a,b),x),IntPolynomialQ(u,x)),Less(Exponent(u,x),C4)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(b_DEFAULT,x_),a_DEFAULT),m_DEFAULT),Power(Plus(Times($p(d,true),x_),c_),n_)),x_Symbol),
    Condition(Map(Function(RegularizeSubst(Slot1,x,Plus(a,Times(b,x)))),Apart(Times(Power(x,m),Power(Plus(c,Times(CN1,a,d,Power(b,CN1)),Times(d,Power(b,CN1),x)),n)),x)),And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n))),Greater(m,C0)),Less(n,C0)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(b_DEFAULT,x_),a_DEFAULT),n_),Power(Plus(Times($p(d,true),x_),c_),n_),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(ExpandExpression(Times(Power(x,m),Power(Plus(Times(a,c),Times(Plus(Times(b,c),Times(a,d)),x),Times(b,d,Power(x,C2))),n)),x),And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n))),Less(n,C0)))),
ISetDelayed(ExpandExpression(Power(Sin(v_),n_),x_Symbol),
    Condition(Expand(TrigReduce(Power(Sin(v),n)),x),And(IntIntegerQ(n),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Power(Cos(v_),n_),x_Symbol),
    Condition(Expand(TrigReduce(Power(Cos(v),n)),x),And(IntIntegerQ(n),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Cos(v_),C2),$p(d,true)),c_DEFAULT),CN1),Power(Sin(v_),n_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(Sin(v),Plus(n,Times(CN1,C2))),Power(d,CN1)),Dist(Times(Plus(c,d),Power(d,CN1)),ExpandExpression(Times(Power(Sin(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Cos(v),C2))),CN1)),x))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Power(Cos(v_),n_),Power(Plus(Times(Power(Sin(v_),C2),$p(d,true)),c_DEFAULT),CN1)),x_Symbol),
    Condition(Plus(Times(CN1,Power(Cos(v),Plus(n,Times(CN1,C2))),Power(d,CN1)),Dist(Times(Plus(c,d),Power(d,CN1)),ExpandExpression(Times(Power(Cos(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Sin(v),C2))),CN1)),x))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Plus(Times(Power(Sin(v_),C2),b_DEFAULT),a_),Power(Plus(Times(Power(Cos(v_),C2),$p(d,true)),c_DEFAULT),CN1)),x_Symbol),
    Condition(Plus(Times(CN1,b,Power(d,CN1)),Times(Plus(Times(b,c),Times(Plus(a,b),d)),Power(Times(d,Plus(c,Times(d,Power(Cos(v),C2)))),CN1))),FreeQ(List(a,b,c,d),x))),
ISetDelayed(ExpandExpression(Times(Plus(Times(Power(Cos(v_),C2),b_DEFAULT),a_),Power(Plus(Times(Power(Sin(v_),C2),$p(d,true)),c_DEFAULT),CN1)),x_Symbol),
    Condition(Plus(Times(CN1,b,Power(d,CN1)),Times(Plus(Times(b,c),Times(Plus(a,b),d)),Power(Times(d,Plus(c,Times(d,Power(Sin(v),C2)))),CN1))),FreeQ(List(a,b,c,d),x))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Sin(v_),n_DEFAULT),a_DEFAULT),Times(Power(Cos(v_),n_DEFAULT),b_DEFAULT)),CN1),Power(Tan(v_),n_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(Sec(v),n),Power(a,CN1)),Times(CN1,b,Power(Times(a,Plus(Times(a,Power(Sin(v),n)),Times(b,Power(Cos(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Cot(v_),n_DEFAULT),Power(Plus(Times(Power(Sin(v_),n_DEFAULT),a_DEFAULT),Times(Power(Cos(v_),n_DEFAULT),b_DEFAULT)),CN1)),x_Symbol),
    Condition(Plus(Times(Power(Csc(v),n),Power(b,CN1)),Times(CN1,a,Power(Times(b,Plus(Times(a,Power(Sin(v),n)),Times(b,Power(Cos(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Cot(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Sec(v_),n_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(Sec(v),n),Power(a,CN1)),Times(CN1,b,Power(Times(a,Plus(Times(a,Power(Sin(v),n)),Times(b,Power(Cos(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Csc(v_),n_DEFAULT),Power(Plus(Times(Power(Tan(v_),n_DEFAULT),b_DEFAULT),a_),CN1)),x_Symbol),
    Condition(Plus(Times(Power(Csc(v),n),Power(a,CN1)),Times(CN1,b,Power(Times(a,Plus(Times(b,Power(Sin(v),n)),Times(a,Power(Cos(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Tan(v_),b_DEFAULT),a_),n_),u_),x_Symbol),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Power(Cos(v),C2),Power(a,CN1)),Times(Cos(v),Sin(v),Power(b,CN1))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Cot(v_),b_DEFAULT),a_),n_),u_),x_Symbol),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Power(Sin(v),C2),Power(a,CN1)),Times(Cos(v),Sin(v),Power(b,CN1))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
ISetDelayed(ExpandExpression(Times(Cos(u_),Sin(u_),v_),x_Symbol),
    Condition(ExpandExpression(Times(v,Rational(C1,C2),Sin(Dist(C2,u))),x),Or(MatchQ(v,Condition(Power(x,m_),RationalQ(m))),MatchQ(v,Condition(Power(f_,w_),And(FreeQ(f,x),LinearQ(w,x))))))),
ISetDelayed(ExpandExpression(Power(Sinh(v_),n_),x_Symbol),
    Condition(Module(List(z),Expand(NormalForm(Subst(TrigReduce(Power(Sinh(z),n)),z,v),x),x)),And(IntIntegerQ(n),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Power(Sinh(v_),n_DEFAULT),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Module(List(z),Expand(Times(Power(x,m),NormalForm(Subst(TrigReduce(Power(Sinh(z),n)),z,v),x)),x)),And(And(FreeQ(m,x),IntIntegerQ(n)),Greater(n,C0)))),
ISetDelayed(ExpandExpression(Power(Cosh(v_),n_),x_Symbol),
    Condition(Module(List(z),Expand(NormalForm(Subst(TrigReduce(Power(Cosh(z),n)),z,v),x),x)),And(IntIntegerQ(n),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Power(Cosh(v_),n_DEFAULT),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Module(List(z),Expand(Times(Power(x,m),NormalForm(Subst(TrigReduce(Power(Cosh(z),n)),z,v),x)),x)),And(And(FreeQ(m,x),IntIntegerQ(n)),Greater(n,C0)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Cosh(v_),C2),$p(d,true)),c_DEFAULT),CN1),Power(Sinh(v_),n_)),x_Symbol),
    Condition(Plus(Times(Power(Sinh(v),Plus(n,Times(CN1,C2))),Power(d,CN1)),Times(CN1,Dist(Times(Plus(c,d),Power(d,CN1)),ExpandExpression(Times(Power(Sinh(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Cosh(v),C2))),CN1)),x)))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Power(Cosh(v_),n_),Power(Plus(Times(Power(Sinh(v_),C2),$p(d,true)),c_DEFAULT),CN1)),x_Symbol),
    Condition(Plus(Times(Power(Cosh(v),Plus(n,Times(CN1,C2))),Power(d,CN1)),Times(CN1,Dist(Times(Plus(c,Times(CN1,d)),Power(d,CN1)),ExpandExpression(Times(Power(Cosh(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Sinh(v),C2))),CN1)),x)))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
ISetDelayed(ExpandExpression(Times(Plus(Times(Power(Sinh(v_),C2),b_DEFAULT),a_),Power(Plus(Times(Power(Cosh(v_),C2),$p(d,true)),c_DEFAULT),CN1)),x_Symbol),
    Condition(Plus(Times(b,Power(d,CN1)),Times(CN1,Plus(Times(b,c),Times(CN1,Plus(a,Times(CN1,b)),d)),Power(Times(d,Plus(c,Times(d,Power(Cosh(v),C2)))),CN1))),FreeQ(List(a,b,c,d),x))),
ISetDelayed(ExpandExpression(Times(Plus(Times(Power(Cosh(v_),C2),b_DEFAULT),a_),Power(Plus(Times(Power(Sinh(v_),C2),$p(d,true)),c_DEFAULT),CN1)),x_Symbol),
    Condition(Plus(Times(b,Power(d,CN1)),Times(CN1,Plus(Times(b,c),Times(CN1,Plus(a,b),d)),Power(Times(d,Plus(c,Times(d,Power(Sinh(v),C2)))),CN1))),FreeQ(List(a,b,c,d),x))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Sinh(v_),n_DEFAULT),a_DEFAULT),Times(Power(Cosh(v_),n_DEFAULT),b_DEFAULT)),CN1),Power(Tanh(v_),n_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(Sech(v),n),Power(a,CN1)),Times(CN1,b,Power(Times(a,Plus(Times(a,Power(Sinh(v),n)),Times(b,Power(Cosh(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Coth(v_),n_DEFAULT),Power(Plus(Times(Power(Sinh(v_),n_DEFAULT),a_DEFAULT),Times(Power(Cosh(v_),n_DEFAULT),b_DEFAULT)),CN1)),x_Symbol),
    Condition(Plus(Times(Power(Csch(v),n),Power(b,CN1)),Times(CN1,a,Power(Times(b,Plus(Times(a,Power(Sinh(v),n)),Times(b,Power(Cosh(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Coth(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Sech(v_),n_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(Sech(v),n),Power(a,CN1)),Times(CN1,b,Power(Times(a,Plus(Times(a,Power(Sinh(v),n)),Times(b,Power(Cosh(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Csch(v_),n_DEFAULT),Power(Plus(Times(Power(Tanh(v_),n_DEFAULT),b_DEFAULT),a_),CN1)),x_Symbol),
    Condition(Plus(Times(Power(Csch(v),n),Power(a,CN1)),Times(CN1,b,Power(Times(a,Plus(Times(b,Power(Sinh(v),n)),Times(a,Power(Cosh(v),n)))),CN1))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Tanh(v_),b_DEFAULT),a_),n_),u_),x_Symbol),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Power(Cosh(v),C2),Power(a,CN1)),Times(CN1,Cosh(v),Sinh(v),Power(b,CN1))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(ExpandExpression(Times(Power(Plus(Times(Coth(v_),b_DEFAULT),a_),n_),u_),x_Symbol),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(CN1,Power(Sinh(v),C2),Power(a,CN1)),Times(Cosh(v),Sinh(v),Power(b,CN1))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(ExpandExpression(Times(Cosh(u_),Sinh(u_),v_),x_Symbol),
    Condition(ExpandExpression(Times(v,Rational(C1,C2),Sinh(Dist(C2,u))),x),Or(MatchQ(v,Condition(Power(x,m_),RationalQ(m))),MatchQ(v,Condition(Power(f_,w_),And(FreeQ(f,x),LinearQ(w,x))))))),
ISetDelayed(ExpandExpression(Times(Power(v_,n_),u_DEFAULT),x_Symbol),
    Condition(Module(List(Set(w,ExpandExpression(u,x))),If(SumQ(w),Map(Function(RegularizeTerm(Times(Slot1,Power(v,n)),x)),w),Times(w,Power(v,n)))),And(FractionQ(n),Or(Less(n,C0),Greater(n,C1)))))
  );
}
