package org.matheclipse.core.integrate.rubi;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * IndefiniteIntegrationRules rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class IndefiniteIntegrationRules12 { 
  public static IAST RULES = List( 
ISetDelayed(Int(Times(Power(Plus(Times(Power(x_,C2),b_DEFAULT),a_),n_),Power(x_,m_DEFAULT),u_),x_Symbol),
    Condition(Dist(Times(CN1,Power(a,n)),Subst(Int(Regularize(Times(Power(Cot(x),m),Power(Sin(x),Times(CN2,Plus(n,C1))),SubstFor(ArcCot(x),u,x)),x),x),x,ArcCot(x))),And(And(And(And(FreeQ(List(a,b),x),FunctionOfQ(ArcCot(x),u,x)),ZeroQ(Plus(a,Times(CN1,b)))),IntIntegerQ(List(m,n))),Less(n,C0)))),
ISetDelayed(Int(Times(Power(Plus(Times(Power(x_,C2),b_DEFAULT),a_),n_),Power(x_,m_DEFAULT),u_),x_Symbol),
    Condition(Dist(Times(CN1,Power(a,n)),Subst(Int(Regularize(Times(Power(Cot(x),m),Power(Sin(x),Times(CN2,Plus(n,C1))),SubstFor(ArcCot(x),u,x)),x),x),x,ArcCot(x))),And(And(And(And(And(And(FreeQ(List(a,b),x),FunctionOfQ(ArcCot(x),u,x)),ZeroQ(Plus(a,Times(CN1,b)))),HalfIntegerQ(n)),Less(n,CN1)),PositiveQ(a)),IntIntegerQ(m)))),
ISetDelayed(Int(ArcCot(u_),x_Symbol),
    Condition(Plus(Times(x,ArcCot(u)),Int(Regularize(Times(x,D(u,x),Power(Plus(C1,Power(u,C2)),CN1)),x),x)),And(InverseFunctionFreeQ(u,x),Not(MatchQ(u,Condition(Plus(c_DEFAULT,Times($p(d,true),Power(f_,Plus(a_DEFAULT,Times(b_DEFAULT,x))))),FreeQ(List(a,b,c,d,f),x))))))),
ISetDelayed(Int(Times(ArcCot(u_),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcCot(u),Power(Plus(m,C1),CN1)),Dist(Power(Plus(m,C1),CN1),Int(Regularize(Times(Power(x,Plus(m,C1)),D(u,x),Power(Plus(C1,Power(u,C2)),CN1)),x),x))),And(And(And(And(FreeQ(m,x),NonzeroQ(Plus(m,C1))),InverseFunctionFreeQ(u,x)),Not(FunctionOfQ(Power(x,Plus(m,C1)),u,x))),FalseQ(PowerVariableExpn(u,Plus(m,C1),x))))),
ISetDelayed(Int(Times(ArcCot(u_),v_),x_Symbol),
    Condition(Module(List(Set(w,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(v,x)))),Condition(Plus(Times(w,ArcCot(u)),Int(Regularize(Times(w,D(u,x),Power(Plus(C1,Power(u,C2)),CN1)),x),x)),InverseFunctionFreeQ(w,x))),And(And(InverseFunctionFreeQ(u,x),Not(MatchQ(v,Condition(Power(x,m_DEFAULT),FreeQ(m,x))))),FalseQ(FunctionOfLinear(Times(v,ArcCot(u)),x))))),
ISetDelayed(Int(Times(ArcCot(Times(b_DEFAULT,x_)),Power(Plus(Times(Power(x_,n_DEFAULT),$p(d,true)),c_),CN1)),x_Symbol),
    Condition(Plus(Dist(Times(Rational(C1,C2),CI),Int(Times(Log(Plus(C1,Times(CN1,CI,Power(Times(b,x),CN1)))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)),Times(CN1,Dist(Times(Rational(C1,C2),CI),Int(Times(Log(Plus(C1,Times(CI,Power(Times(b,x),CN1)))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)))),And(And(FreeQ(List(b,c,d),x),IntIntegerQ(n)),Not(And(Equal(n,C2),ZeroQ(Plus(Times(Power(b,C2),c),Times(CN1,d)))))))),
ISetDelayed(Int(Times(ArcCot(Plus(Times(b_DEFAULT,x_),a_)),Power(Plus(Times(Power(x_,n_DEFAULT),$p(d,true)),c_),CN1)),x_Symbol),
    Condition(Plus(Dist(Times(Rational(C1,C2),CI),Int(Times(Log(Plus(C1,Times(CN1,CI,Power(Plus(a,Times(b,x)),CN1)))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)),Times(CN1,Dist(Times(Rational(C1,C2),CI),Int(Times(Log(Plus(C1,Times(CI,Power(Plus(a,Times(b,x)),CN1)))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)))),And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(n)),Not(And(Equal(n,C1),ZeroQ(Plus(Times(a,d),Times(CN1,b,c)))))))),
ISetDelayed(Int(ArcSec(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),ArcSec(Plus(a,Times(b,x))),Power(b,CN1)),Times(CN1,Int(Power(Times(Plus(a,Times(b,x)),Sqrt(Plus(C1,Times(CN1,Power(Power(Plus(a,Times(b,x)),C2),CN1))))),CN1),x))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Times(ArcSec(Times(Power(x_,n_DEFAULT),a_DEFAULT)),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(CI,Power(ArcSec(Times(a,Power(x,n))),C2),Power(Times(C2,n),CN1)),Times(CN1,ArcSec(Times(a,Power(x,n))),Log(Plus(C1,Times(CN1,Power(Power(Plus(Times(CI,Power(Times(Power(x,n),a),CN1)),Sqrt(Plus(C1,Times(CN1,Power(Times(Power(x,Times(C2,n)),Power(a,C2)),CN1))))),C2),CN1)))),Power(n,CN1)),Times(CI,PolyLog(C2,Power(Power(Plus(Times(CI,Power(Times(Power(x,n),a),CN1)),Sqrt(Plus(C1,Times(CN1,Power(Times(Power(x,Times(C2,n)),Power(a,C2)),CN1))))),C2),CN1)),Power(Times(C2,n),CN1))),FreeQ(List(a,n),x))),
ISetDelayed(Int(Times(ArcSec(Plus(Times(b_DEFAULT,x_),a_)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Dist(Power(b,CN1),Subst(Int(Times(Power(Plus(Times(CN1,a,Power(b,CN1)),Times(x,Power(b,CN1))),m),ArcSec(x)),x),x,Plus(a,Times(b,x)))),And(And(FreeQ(List(a,b),x),IntIntegerQ(m)),Greater(m,C0)))),
ISetDelayed(Int(Times(ArcSec(Times(a_DEFAULT,x_)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcSec(Times(a,x)),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Power(Times(a,Plus(m,C1)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,C1))),Power(Sqrt(Plus(C1,Times(CN1,Power(Power(Times(a,x),C2),CN1)))),CN1)),x)))),And(FreeQ(List(a,m),x),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Times(ArcSec(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcSec(Plus(a,Times(b,x))),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Times(b,Power(Plus(m,C1),CN1)),Int(Times(Power(x,Plus(m,C1)),Power(Times(Sqrt(Plus(C1,Times(CN1,Power(Power(Plus(a,Times(b,x)),C2),CN1)))),Power(Plus(a,Times(b,x)),C2)),CN1)),x)))),And(FreeQ(List(a,b,m),x),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Times(Power(ArcSec(Times(Power(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT),CN1),c_DEFAULT)),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(ArcCos(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(ArcSec(u_),x_Symbol),
    Condition(Plus(Times(x,ArcSec(u)),Times(CN1,Int(Regularize(Times(x,D(u,x),Power(Times(Power(u,C2),Sqrt(Plus(C1,Times(CN1,Power(Power(u,C2),CN1))))),CN1)),x),x))),And(InverseFunctionFreeQ(u,x),Not(MatchQ(u,Condition(Plus(c_DEFAULT,Times($p(d,true),Power(f_,Plus(a_DEFAULT,Times(b_DEFAULT,x))))),FreeQ(List(a,b,c,d,f),x))))))),
ISetDelayed(Int(ArcCsc(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),ArcCsc(Plus(a,Times(b,x))),Power(b,CN1)),Int(Power(Times(Plus(a,Times(b,x)),Sqrt(Plus(C1,Times(CN1,Power(Power(Plus(a,Times(b,x)),C2),CN1))))),CN1),x)),FreeQ(List(a,b),x))),
ISetDelayed(Int(Times(ArcCsc(Times(Power(x_,n_DEFAULT),a_DEFAULT)),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(CI,Power(ArcCsc(Times(a,Power(x,n))),C2),Power(Times(C2,n),CN1)),Times(CN1,ArcCsc(Times(a,Power(x,n))),Log(Plus(C1,Times(CN1,Power(Plus(Times(CI,Power(Times(Power(x,n),a),CN1)),Sqrt(Plus(C1,Times(CN1,Power(Times(Power(x,Times(C2,n)),Power(a,C2)),CN1))))),C2)))),Power(n,CN1)),Times(CI,PolyLog(C2,Power(Plus(Times(CI,Power(Times(Power(x,n),a),CN1)),Sqrt(Plus(C1,Times(CN1,Power(Times(Power(x,Times(C2,n)),Power(a,C2)),CN1))))),C2)),Power(Times(C2,n),CN1))),FreeQ(List(a,n),x))),
ISetDelayed(Int(Times(ArcCsc(Plus(Times(b_DEFAULT,x_),a_)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Dist(Power(b,CN1),Subst(Int(Times(Power(Plus(Times(CN1,a,Power(b,CN1)),Times(x,Power(b,CN1))),m),ArcCsc(x)),x),x,Plus(a,Times(b,x)))),And(And(FreeQ(List(a,b),x),IntIntegerQ(m)),Greater(m,C0)))),
ISetDelayed(Int(Times(ArcCsc(Times(a_DEFAULT,x_)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcCsc(Times(a,x)),Power(Plus(m,C1),CN1)),Dist(Power(Times(a,Plus(m,C1)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,C1))),Power(Sqrt(Plus(C1,Times(CN1,Power(Power(Times(a,x),C2),CN1)))),CN1)),x))),And(FreeQ(List(a,m),x),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Times(ArcCsc(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcCsc(Plus(a,Times(b,x))),Power(Plus(m,C1),CN1)),Dist(Times(b,Power(Plus(m,C1),CN1)),Int(Times(Power(x,Plus(m,C1)),Power(Times(Sqrt(Plus(C1,Times(CN1,Power(Power(Plus(a,Times(b,x)),C2),CN1)))),Power(Plus(a,Times(b,x)),C2)),CN1)),x))),And(FreeQ(List(a,b,m),x),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Times(Power(ArcCsc(Times(Power(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT),CN1),c_DEFAULT)),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(ArcSin(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(ArcCsc(u_),x_Symbol),
    Condition(Plus(Times(x,ArcCsc(u)),Int(Regularize(Times(x,D(u,x),Power(Times(Power(u,C2),Sqrt(Plus(C1,Times(CN1,Power(Power(u,C2),CN1))))),CN1)),x),x)),And(InverseFunctionFreeQ(u,x),Not(MatchQ(u,Condition(Plus(c_DEFAULT,Times($p(d,true),Power(f_,Plus(a_DEFAULT,Times(b_DEFAULT,x))))),FreeQ(List(a,b,c,d,f),x))))))),
ISetDelayed(Int(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),ArcSinh(Plus(a,Times(b,x))),Power(b,CN1)),Times(CN1,Sqrt(Plus(C1,Power(Plus(a,Times(b,x)),C2))),Power(b,CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),CN1),x_Symbol),
    Condition(Times(CoshIntegral(ArcSinh(Plus(a,Times(b,x)))),Power(b,CN1)),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),CN2),x_Symbol),
    Condition(Plus(Times(CN1,Sqrt(Plus(C1,Power(Plus(a,Times(b,x)),C2))),Power(Times(b,ArcSinh(Plus(a,Times(b,x)))),CN1)),Times(SinhIntegral(ArcSinh(Plus(a,Times(b,x)))),Power(b,CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),CN1D2),x_Symbol),
    Condition(Times(Rational(C1,C2),Sqrt(Pi),Plus(Erf(Sqrt(ArcSinh(Plus(a,Times(b,x))))),Erfi(Sqrt(ArcSinh(Plus(a,Times(b,x)))))),Power(b,CN1)),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),C1D2),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),Sqrt(ArcSinh(Plus(a,Times(b,x)))),Power(b,CN1)),Times(CN1,Rational(C1,C4),Sqrt(Pi),Plus(Times(CN1,Erf(Sqrt(ArcSinh(Plus(a,Times(b,x)))))),Erfi(Sqrt(ArcSinh(Plus(a,Times(b,x)))))),Power(b,CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),Power(ArcSinh(Plus(a,Times(b,x))),n),Power(b,CN1)),Times(CN1,n,Sqrt(Plus(C1,Power(Plus(a,Times(b,x)),C2))),Power(ArcSinh(Plus(a,Times(b,x))),Plus(n,Times(CN1,C1))),Power(b,CN1)),Dist(Times(n,Plus(n,Times(CN1,C1))),Int(Power(ArcSinh(Plus(a,Times(b,x))),Plus(n,Times(CN1,C2))),x))),And(And(FreeQ(List(a,b),x),RationalQ(n)),Greater(n,C1)))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_Symbol),
    Condition(Plus(Times(CN1,Plus(a,Times(b,x)),Power(ArcSinh(Plus(a,Times(b,x))),Plus(n,C2)),Power(Times(b,Plus(n,C1),Plus(n,C2)),CN1)),Times(Sqrt(Plus(C1,Power(Plus(a,Times(b,x)),C2))),Power(ArcSinh(Plus(a,Times(b,x))),Plus(n,C1)),Power(Times(b,Plus(n,C1)),CN1)),Dist(Power(Times(Plus(n,C1),Plus(n,C2)),CN1),Int(Power(ArcSinh(Plus(a,Times(b,x))),Plus(n,C2)),x))),And(And(And(FreeQ(List(a,b),x),RationalQ(n)),Less(n,CN1)),Unequal(n,CN2)))),
ISetDelayed(Int(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_Symbol),
    Condition(Plus(Times(Power(ArcSinh(Plus(a,Times(b,x))),n),Gamma(Plus(n,C1),Times(CN1,ArcSinh(Plus(a,Times(b,x))))),Power(Times(C2,b,Power(Times(CN1,ArcSinh(Plus(a,Times(b,x)))),n)),CN1)),Times(CN1,Gamma(Plus(n,C1),ArcSinh(Plus(a,Times(b,x)))),Power(Times(C2,b),CN1))),And(FreeQ(List(a,b,n),x),Or(Not(RationalQ(n)),Less(Less(CN1,n),C1))))),
ISetDelayed(Int(Times(ArcSinh(Times(Power(x_,n_DEFAULT),a_DEFAULT)),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(Power(ArcSinh(Times(a,Power(x,n))),C2),Power(Times(C2,n),CN1)),Times(ArcSinh(Times(a,Power(x,n))),Log(Plus(C1,Times(CN1,Power(E,Times(CN2,ArcSinh(Times(a,Power(x,n)))))))),Power(n,CN1)),Times(CN1,PolyLog(C2,Power(E,Times(CN2,ArcSinh(Times(a,Power(x,n)))))),Power(Times(C2,n),CN1))),FreeQ(List(a,n),x))),
ISetDelayed(Int(Times(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcSinh(Plus(a,Times(b,x))),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Times(b,Power(Plus(m,C1),CN1)),Int(Times(Power(x,Plus(m,C1)),Power(Sqrt(Plus(C1,Power(a,C2),Times(C2,a,b,x),Times(Power(b,C2),Power(x,C2)))),CN1)),x)))),And(FreeQ(List(a,b,m),x),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Power(Plus(Times(ArcSinh(Plus(Times($p(d,true),x_),c_DEFAULT)),b_DEFAULT),a_),CN1D2),x_Symbol),
    Condition(Plus(Times(Sqrt(Pi),Power(E,Times(a,Power(b,CN1))),Erf(Times(Sqrt(Plus(a,Times(b,ArcSinh(Plus(c,Times(d,x)))))),Power(Rt(b,C2),CN1))),Power(Times(C2,Rt(b,C2),d),CN1)),Times(Sqrt(Pi),Power(E,Times(CN1,a,Power(b,CN1))),Erfi(Times(Sqrt(Plus(a,Times(b,ArcSinh(Plus(c,Times(d,x)))))),Power(Rt(b,C2),CN1))),Power(Times(C2,Rt(b,C2),d),CN1))),And(FreeQ(List(a,b,c,d),x),PosQ(b)))),
ISetDelayed(Int(Power(Plus(Times(ArcSinh(Plus(Times($p(d,true),x_),c_DEFAULT)),b_DEFAULT),a_),CN1D2),x_Symbol),
    Condition(Plus(Times(CN1,Sqrt(Pi),Power(E,Times(CN1,a,Power(b,CN1))),Erf(Times(Sqrt(Plus(a,Times(b,ArcSinh(Plus(c,Times(d,x)))))),Power(Rt(Times(CN1,b),C2),CN1))),Power(Times(C2,Rt(Times(CN1,b),C2),d),CN1)),Times(CN1,Sqrt(Pi),Power(E,Times(a,Power(b,CN1))),Erfi(Times(Sqrt(Plus(a,Times(b,ArcSinh(Plus(c,Times(d,x)))))),Power(Rt(Times(CN1,b),C2),CN1))),Power(Times(C2,Rt(Times(CN1,b),C2),d),CN1))),And(FreeQ(List(a,b,c,d),x),NegQ(b)))),
ISetDelayed(Int(Times(Power(ArcSinh(x_),n_DEFAULT),Power(Plus(C1,Power(x_,C2)),m_DEFAULT)),x_Symbol),
    Condition(Module(List(Set(u,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(Power(Plus(C1,Power(x,C2)),m),x)))),Plus(Times(u,Power(ArcSinh(x),n)),Times(CN1,Dist(n,Int(Expand(Times(u,Power(ArcSinh(x),Plus(n,Times(CN1,C1))),Power(Sqrt(Plus(C1,Power(x,C2))),CN1))),x))))),And(And(And(HalfIntegerQ(m),Unequal(m,Rational(CN1,C2))),IntIntegerQ(n)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),Power(u_,CN1D2),x_),x_Symbol),
    Condition(Plus(Times(Sqrt(u),Power(ArcSinh(Plus(a,Times(b,x))),n),Power(Power(b,C2),CN1)),Times(CN1,Dist(Times(n,Power(b,CN1)),Int(Power(ArcSinh(Plus(a,Times(b,x))),Plus(n,Times(CN1,C1))),x))),Times(CN1,Dist(Times(a,Power(b,CN1)),Int(Times(Power(ArcSinh(Plus(a,Times(b,x))),n),Power(Sqrt(u),CN1)),x)))),And(And(And(FreeQ(List(a,b),x),ZeroQ(Plus(u,Times(CN1,C1),Times(CN1,Power(Plus(a,Times(b,x)),C2))))),RationalQ(n)),Greater(n,C1)))),
ISetDelayed(Int(Times(Power(ArcSinh(Times(Power(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT),CN1),c_DEFAULT)),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(ArcCsch(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(ArcSinh(u_),x_Symbol),
    Condition(Plus(Times(x,ArcSinh(u)),Times(CN1,Int(Regularize(Times(x,D(u,x),Power(Sqrt(Plus(C1,Power(u,C2))),CN1)),x),x))),And(InverseFunctionFreeQ(u,x),Not(MatchQ(u,Condition(Plus(c_DEFAULT,Times($p(d,true),Power(f_,Plus(a_DEFAULT,Times(b_DEFAULT,x))))),FreeQ(List(a,b,c,d,f),x))))))),
ISetDelayed(Int(Power(f_,Times(ArcSinh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),c_DEFAULT)),x_Symbol),
    Condition(Times(Power(f,Times(c,ArcSinh(Plus(a,Times(b,x))))),Plus(a,Times(b,x),Times(CN1,c,Sqrt(Plus(C1,Power(Plus(a,Times(b,x)),C2))),Log(f))),Power(Times(b,Plus(C1,Times(CN1,Power(c,C2),Power(Log(f),C2)))),CN1)),And(FreeQ(List(a,b,c,f),x),NonzeroQ(Plus(C1,Times(CN1,Power(c,C2),Power(Log(f),C2))))))),
ISetDelayed(Int(Power(E,Times(ArcSinh(v_),n_DEFAULT)),x_Symbol),
    Condition(Int(Power(Plus(v,Sqrt(Plus(C1,Power(v,C2)))),n),x),And(IntIntegerQ(n),IntPolynomialQ(v,x)))),
ISetDelayed(Int(Times(Power(E,Times(ArcSinh(v_),n_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Int(Times(Power(x,m),Power(Plus(v,Sqrt(Plus(C1,Power(v,C2)))),n)),x),And(And(RationalQ(m),IntIntegerQ(n)),IntPolynomialQ(v,x)))),
ISetDelayed(Int(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),ArcCosh(Plus(a,Times(b,x))),Power(b,CN1)),Times(CN1,Sqrt(Plus(CN1,a,Times(b,x))),Sqrt(Plus(C1,a,Times(b,x))),Power(b,CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),CN1),x_Symbol),
    Condition(Times(SinhIntegral(ArcCosh(Plus(a,Times(b,x)))),Power(b,CN1)),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),CN2),x_Symbol),
    Condition(Plus(Times(CN1,Sqrt(Plus(CN1,a,Times(b,x))),Sqrt(Plus(C1,a,Times(b,x))),Power(Times(b,ArcCosh(Plus(a,Times(b,x)))),CN1)),Times(CoshIntegral(ArcCosh(Plus(a,Times(b,x)))),Power(b,CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),CN1D2),x_Symbol),
    Condition(Times(Rational(C1,C2),Sqrt(Pi),Plus(Times(CN1,Erf(Sqrt(ArcCosh(Plus(a,Times(b,x)))))),Erfi(Sqrt(ArcCosh(Plus(a,Times(b,x)))))),Power(b,CN1)),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),C1D2),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),Sqrt(ArcCosh(Plus(a,Times(b,x)))),Power(b,CN1)),Times(CN1,Rational(C1,C4),Sqrt(Pi),Plus(Erf(Sqrt(ArcCosh(Plus(a,Times(b,x))))),Erfi(Sqrt(ArcCosh(Plus(a,Times(b,x)))))),Power(b,CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),Power(ArcCosh(Plus(a,Times(b,x))),n),Power(b,CN1)),Times(CN1,n,Sqrt(Plus(CN1,a,Times(b,x))),Sqrt(Plus(C1,a,Times(b,x))),Power(ArcCosh(Plus(a,Times(b,x))),Plus(n,Times(CN1,C1))),Power(b,CN1)),Dist(Times(n,Plus(n,Times(CN1,C1))),Int(Power(ArcCosh(Plus(a,Times(b,x))),Plus(n,Times(CN1,C2))),x))),And(And(FreeQ(List(a,b),x),RationalQ(n)),Greater(n,C1)))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_Symbol),
    Condition(Plus(Times(CN1,Plus(a,Times(b,x)),Power(ArcCosh(Plus(a,Times(b,x))),Plus(n,C2)),Power(Times(b,Plus(n,C1),Plus(n,C2)),CN1)),Times(Sqrt(Plus(CN1,a,Times(b,x))),Sqrt(Plus(C1,a,Times(b,x))),Power(ArcCosh(Plus(a,Times(b,x))),Plus(n,C1)),Power(Times(b,Plus(n,C1)),CN1)),Dist(Power(Times(Plus(n,C1),Plus(n,C2)),CN1),Int(Power(ArcCosh(Plus(a,Times(b,x))),Plus(n,C2)),x))),And(And(And(FreeQ(List(a,b),x),RationalQ(n)),Less(n,CN1)),Unequal(n,CN2)))),
ISetDelayed(Int(Power(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_Symbol),
    Condition(Plus(Times(Power(ArcCosh(Plus(a,Times(b,x))),n),Gamma(Plus(n,C1),Times(CN1,ArcCosh(Plus(a,Times(b,x))))),Power(Times(C2,b,Power(Times(CN1,ArcCosh(Plus(a,Times(b,x)))),n)),CN1)),Times(Gamma(Plus(n,C1),ArcCosh(Plus(a,Times(b,x)))),Power(Times(C2,b),CN1))),And(FreeQ(List(a,b,n),x),Or(Not(RationalQ(n)),Less(Less(CN1,n),C1))))),
ISetDelayed(Int(Times(ArcCosh(Times(Power(x_,n_DEFAULT),a_DEFAULT)),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(Power(ArcCosh(Times(a,Power(x,n))),C2),Power(Times(C2,n),CN1)),Times(ArcCosh(Times(a,Power(x,n))),Log(Plus(C1,Power(E,Times(CN2,ArcCosh(Times(a,Power(x,n))))))),Power(n,CN1)),Times(CN1,PolyLog(C2,Times(CN1,Power(E,Times(CN2,ArcCosh(Times(a,Power(x,n))))))),Power(Times(C2,n),CN1))),FreeQ(List(a,n),x))),
ISetDelayed(Int(Times(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcCosh(Plus(a,Times(b,x))),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Times(b,Power(Plus(m,C1),CN1)),Int(Times(Power(x,Plus(m,C1)),Power(Times(Sqrt(Plus(CN1,a,Times(b,x))),Sqrt(Plus(C1,a,Times(b,x)))),CN1)),x)))),And(FreeQ(List(a,b,m),x),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Power(Plus(Times(ArcCosh(Plus(Times($p(d,true),x_),c_DEFAULT)),b_DEFAULT),a_),CN1D2),x_Symbol),
    Condition(Plus(Times(Sqrt(Pi),Power(E,Times(CN1,a,Power(b,CN1))),Erfi(Times(Sqrt(Plus(a,Times(b,ArcCosh(Plus(c,Times(d,x)))))),Power(Rt(b,C2),CN1))),Power(Times(C2,Rt(b,C2),d),CN1)),Times(CN1,Sqrt(Pi),Power(E,Times(a,Power(b,CN1))),Erf(Times(Sqrt(Plus(a,Times(b,ArcCosh(Plus(c,Times(d,x)))))),Power(Rt(b,C2),CN1))),Power(Times(C2,Rt(b,C2),d),CN1))),And(FreeQ(List(a,b,c,d),x),PosQ(b)))),
ISetDelayed(Int(Power(Plus(Times(ArcCosh(Plus(Times($p(d,true),x_),c_DEFAULT)),b_DEFAULT),a_),CN1D2),x_Symbol),
    Condition(Plus(Times(CN1,Sqrt(Pi),Power(E,Times(CN1,a,Power(b,CN1))),Erf(Times(Sqrt(Plus(a,Times(b,ArcCosh(Plus(c,Times(d,x)))))),Power(Rt(Times(CN1,b),C2),CN1))),Power(Times(C2,Rt(Times(CN1,b),C2),d),CN1)),Times(Sqrt(Pi),Power(E,Times(a,Power(b,CN1))),Erfi(Times(Sqrt(Plus(a,Times(b,ArcCosh(Plus(c,Times(d,x)))))),Power(Rt(Times(CN1,b),C2),CN1))),Power(Times(C2,Rt(Times(CN1,b),C2),d),CN1))),And(FreeQ(List(a,b,c,d),x),NegQ(b)))),
ISetDelayed(Int(Times(Power(ArcCosh(x_),n_DEFAULT),Power(Plus(CN1,x_),m_DEFAULT),Power(Plus(C1,x_),m_DEFAULT)),x_Symbol),
    Condition(Module(List(Set(u,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(Times(Power(Plus(C1,x),m),Power(Plus(CN1,x),m)),x)))),Plus(Times(u,Power(ArcCosh(x),n)),Times(CN1,Dist(n,Int(Expand(Times(u,Power(ArcCosh(x),Plus(n,Times(CN1,C1))),Power(Times(Sqrt(Plus(C1,x)),Sqrt(Plus(CN1,x))),CN1))),x))))),And(And(And(HalfIntegerQ(m),Unequal(m,Rational(CN1,C2))),IntIntegerQ(n)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcCosh(Times(Power(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT),CN1),c_DEFAULT)),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(ArcSech(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(ArcCosh(u_),x_Symbol),
    Condition(Plus(Times(x,ArcCosh(u)),Times(CN1,Int(Regularize(Times(x,D(u,x),Power(Times(Sqrt(Plus(CN1,u)),Sqrt(Plus(C1,u))),CN1)),x),x))),And(InverseFunctionFreeQ(u,x),Not(MatchQ(u,Condition(Plus(c_DEFAULT,Times($p(d,true),Power(f_,Plus(a_DEFAULT,Times(b_DEFAULT,x))))),FreeQ(List(a,b,c,d,f),x))))))),
ISetDelayed(Int(Power(f_,Times(ArcCosh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),c_DEFAULT)),x_Symbol),
    Condition(Times(Power(f,Times(c,ArcCosh(Plus(a,Times(b,x))))),Plus(a,Times(b,x),Times(CN1,c,Sqrt(Times(Plus(CN1,a,Times(b,x)),Power(Plus(C1,a,Times(b,x)),CN1))),Plus(C1,a,Times(b,x)),Log(f))),Power(Times(b,Plus(C1,Times(CN1,Power(c,C2),Power(Log(f),C2)))),CN1)),And(FreeQ(List(a,b,c,f),x),NonzeroQ(Plus(C1,Times(CN1,Power(c,C2),Power(Log(f),C2))))))),
ISetDelayed(Int(Power(E,Times(ArcCosh(v_),n_DEFAULT)),x_Symbol),
    Condition(Int(Power(Plus(v,Times(Sqrt(Plus(CN1,v)),Sqrt(Plus(C1,v)))),n),x),And(IntIntegerQ(n),IntPolynomialQ(v,x)))),
ISetDelayed(Int(Times(Power(E,Times(ArcCosh(v_),n_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Int(Times(Power(x,m),Power(Plus(v,Times(Sqrt(Plus(CN1,v)),Sqrt(Plus(C1,v)))),n)),x),And(And(RationalQ(m),IntIntegerQ(n)),IntPolynomialQ(v,x)))),
ISetDelayed(Int(ArcTanh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),ArcTanh(Plus(a,Times(b,x))),Power(b,CN1)),Times(Log(Plus(C1,Times(CN1,Power(Plus(a,Times(b,x)),C2)))),Power(Times(C2,b),CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(ArcTanh(Plus(Times(Power(x_,n_),b_DEFAULT),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(x,ArcTanh(Plus(a,Times(b,Power(x,n))))),Times(CN1,Dist(Times(b,n),Int(Times(Power(x,n),Power(Plus(C1,Times(CN1,Power(a,C2)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Power(b,C2),Power(x,Times(C2,n)))),CN1)),x)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(Int(Times(ArcTanh(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT)),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,a,Times(b,Power(x,n)))),Power(x,CN1)),x)),Times(CN1,Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,Times(CN1,a),Times(CN1,b,Power(x,n)))),Power(x,CN1)),x)))),FreeQ(List(a,b,n),x))),
ISetDelayed(Int(Times(ArcTanh(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcTanh(Plus(a,Times(b,Power(x,n)))),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Times(b,n,Power(Plus(m,C1),CN1)),Int(Times(Power(x,Plus(m,n)),Power(Plus(C1,Times(CN1,Power(a,C2)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Power(b,C2),Power(x,Times(C2,n)))),CN1)),x)))),And(And(And(FreeQ(List(a,b,m),x),IntIntegerQ(n)),NonzeroQ(Plus(m,C1))),NonzeroQ(Plus(m,Times(CN1,n),C1))))),
ISetDelayed(Int(Times(Power(ArcTanh(x_),n_DEFAULT),Power(Plus(C1,Times(CN1,Power(x_,C2))),m_)),x_Symbol),
    Condition(Module(List(Set(u,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(Power(Plus(C1,Times(CN1,Power(x,C2))),m),x)))),Plus(Times(u,Power(ArcTanh(x),n)),Times(CN1,Dist(n,Int(Expand(Times(u,Power(ArcTanh(x),Plus(n,Times(CN1,C1))),Power(Plus(C1,Times(CN1,Power(x,C2))),CN1))),x))))),And(And(IntIntegerQ(List(m,n)),Less(m,CN1)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcTanh(x_),n_DEFAULT),Power(Plus(CN1,Power(x_,C2)),m_)),x_Symbol),
    Condition(Dist(Power(CN1,m),Int(Times(Power(Plus(C1,Times(CN1,Power(x,C2))),m),Power(ArcTanh(x),n)),x)),And(And(IntIntegerQ(List(m,n)),Less(m,CN1)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),CN1),Power(ArcTanh(x_),CN1),Power(Plus(C1,Times(CN1,Power(x_,C2))),CN1)),x_Symbol),
    Times(Plus(Times(CN1,Log(ArcCoth(x))),Log(ArcTanh(x))),Power(Plus(ArcCoth(x),Times(CN1,ArcTanh(x))),CN1))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),n_DEFAULT),Power(ArcTanh(x_),p_DEFAULT),Power(Plus(C1,Times(CN1,Power(x_,C2))),CN1)),x_Symbol),
    Condition(Plus(Times(Power(ArcCoth(x),Plus(n,C1)),Power(ArcTanh(x),p),Power(Plus(n,C1),CN1)),Times(CN1,Dist(Times(p,Power(Plus(n,C1),CN1)),Int(Times(Power(ArcCoth(x),Plus(n,C1)),Power(ArcTanh(x),Plus(p,Times(CN1,C1))),Power(Plus(C1,Times(CN1,Power(x,C2))),CN1)),x)))),And(IntIntegerQ(List(n,p)),And(Less(C0,p),LessEqual(p,n))))),
ISetDelayed(Int(Times(Power(ArcTanh(Times(a_DEFAULT,x_)),n_),x_),x_Symbol),
    Condition(Plus(Times(Plus(CN1,Times(Power(a,C2),Power(x,C2))),Power(ArcTanh(Times(a,x)),n),Power(Times(C2,Power(a,C2)),CN1)),Dist(Times(n,Power(Times(C2,a),CN1)),Int(Power(ArcTanh(Times(a,x)),Plus(n,Times(CN1,C1))),x))),And(And(FreeQ(a,x),RationalQ(n)),Greater(n,C1)))),
ISetDelayed(Int(Times(Power(ArcTanh(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_),x_Symbol),
    Condition(Plus(Times(CN1,Plus(C1,Times(CN1,Power(Plus(a,Times(b,x)),C2))),Power(ArcTanh(Plus(a,Times(b,x))),n),Power(Times(C2,Power(b,C2)),CN1)),Dist(Times(n,Power(Times(C2,b),CN1)),Int(Power(ArcTanh(Plus(a,Times(b,x))),Plus(n,Times(CN1,C1))),x)),Times(CN1,Dist(Times(a,Power(b,CN1)),Int(Power(ArcTanh(Plus(a,Times(b,x))),n),x)))),And(And(FreeQ(List(a,b),x),RationalQ(n)),Greater(n,C1)))),
ISetDelayed(Int(Times(Power(ArcTanh(Times(Power(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT),CN1),c_DEFAULT)),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(ArcCoth(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(Times(Power(v_,n_DEFAULT),u_),x_Symbol),
    Condition(Module(List(Set($s("tmp"),InverseFunctionOfLinear(u,x))),Condition(Dist(Times(Power(Times(CN1,Discriminant(v,x),Power(Times(C4,Coefficient(v,x,C2)),CN1)),n),Power(Coefficient(Part($s("tmp"),C1),x,C1),CN1)),Subst(Int(Regularize(Times(SubstForInverseFunction(u,$s("tmp"),x),Power(Sech(x),Times(C2,Plus(n,C1)))),x),x),x,$s("tmp"))),And(And(NotFalseQ($s("tmp")),SameQ(Head($s("tmp")),$s("ArcTanh"))),ZeroQ(Plus(Times(Discriminant(v,x),Power(Part($s("tmp"),C1),C2)),Times(CN1,Power(D(v,x),C2))))))),And(And(And(QuadraticQ(v,x),IntIntegerQ(n)),Less(n,C0)),PosQ(Discriminant(v,x))))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(Plus(C1,v),Times(Rational(C1,C2),n)),Power(Power(Plus(C1,Times(CN1,v)),Times(Rational(C1,C2),n)),CN1)),x),EvenQ(n))),
ISetDelayed(Int(Power(E,Times(ArcTanh(v_),n_DEFAULT)),x_Symbol),
    Condition(Int(Times(Power(Plus(C1,v),Times(Rational(C1,C2),n)),Power(Power(Plus(C1,Times(CN1,v)),Times(Rational(C1,C2),n)),CN1)),x),RationalQ(n))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Int(Times(Power(x,m),Power(Plus(C1,v),n),Power(Power(Plus(C1,Times(CN1,Power(v,C2))),Times(Rational(C1,C2),n)),CN1)),x),And(And(RationalQ(m),OddQ(n)),IntPolynomialQ(v,x)))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(C1,Times(CN1,Power(v_,C2))),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(Plus(C1,Times(CN1,v)),Plus(m,Times(CN1,Rational(C1,C2),n))),Power(Plus(C1,v),Plus(m,Times(Rational(C1,C2),n)))),x),And(And(RationalQ(List(m,n)),IntIntegerQ(Plus(m,Times(CN1,Rational(C1,C2),n)))),IntIntegerQ(Plus(m,Times(Rational(C1,C2),n)))))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(Times(Power(v_,C2),b_DEFAULT),a_),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Times(Power(Plus(a,Times(b,Power(v,C2))),m),Power(Power(Plus(C1,Times(CN1,Power(v,C2))),m),CN1),Int(Times(u,Power(Plus(C1,Times(CN1,v)),Plus(m,Times(CN1,Rational(C1,C2),n))),Power(Plus(C1,v),Plus(m,Times(Rational(C1,C2),n)))),x)),And(And(And(And(FreeQ(List(a,b),x),ZeroQ(Plus(a,b))),RationalQ(List(m,n))),IntIntegerQ(Plus(m,Times(CN1,Rational(C1,C2),n)))),IntIntegerQ(Plus(m,Times(Rational(C1,C2),n)))))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(C1,Times(CN1,Power(v_,C2))),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(Plus(C1,v),n),Power(Plus(C1,Times(CN1,Power(v,C2))),Plus(m,Times(CN1,Rational(C1,C2),n)))),x),And(And(RationalQ(n),IntIntegerQ(m)),Greater(m,C0)))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(C1,v_),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(Plus(C1,v),Plus(m,n)),Power(Power(Plus(C1,Times(CN1,Power(v,C2))),Times(Rational(C1,C2),n)),CN1)),x),And(RationalQ(List(m,n)),IntIntegerQ(Plus(m,n))))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(C1,v_),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(Plus(C1,v),Plus(m,Times(Rational(C1,C2),n))),Power(Power(Plus(C1,Times(CN1,v)),Times(Rational(C1,C2),n)),CN1)),x),RationalQ(List(m,n)))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(C1,Times(CN1,v_)),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Int(Times(u,Power(Plus(C1,v),Times(Rational(C1,C2),n)),Power(Plus(C1,Times(CN1,v)),Plus(m,Times(CN1,Rational(C1,C2),n)))),x),RationalQ(List(m,n)))),
ISetDelayed(Int(Times(Power(E,Times(ArcTanh(v_),n_DEFAULT)),Power(Plus(Times(b_DEFAULT,v_),a_),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Dist(Power(a,m),Int(Times(u,Power(E,Times(n,ArcTanh(v))),Power(Plus(C1,Times(b,Power(a,CN1),v)),m)),x)),And(And(And(And(FreeQ(List(a,b),x),IntIntegerQ(m)),RationalQ(n)),NonzeroQ(Plus(a,Times(CN1,C1)))),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(Int(Times(Power(E,ArcTanh(v_)),Power(Plus(Times(Power(v_,CN2),b_DEFAULT),a_),m_DEFAULT),u_DEFAULT),x_Symbol),
    Condition(Plus(Times(Power(b,m),Int(Times(u,Power(Plus(C1,Times(CN1,Power(v,C2))),Plus(m,Times(CN1,Rational(C1,C2)))),Power(Power(v,Times(C2,m)),CN1)),x)),Times(Power(b,m),Int(Times(u,Power(Plus(C1,Times(CN1,Power(v,C2))),Plus(m,Times(CN1,Rational(C1,C2)))),Power(Power(v,Plus(Times(C2,m),Times(CN1,C1))),CN1)),x))),And(And(FreeQ(List(a,b),x),ZeroQ(Plus(a,b))),IntIntegerQ(m)))),
ISetDelayed(Int(Times(Power(Plus(Times(Power(x_,C2),b_DEFAULT),a_),n_),u_),x_Symbol),
    Condition(Dist(Power(a,n),Subst(Int(Regularize(Times(Power(Cosh(x),Times(CN2,Plus(n,C1))),SubstFor(ArcTanh(x),u,x)),x),x),x,ArcTanh(x))),And(And(And(And(And(FreeQ(List(a,b),x),FunctionOfQ(ArcTanh(x),u,x)),ZeroQ(Plus(a,b))),HalfIntegerQ(n)),Less(n,CN1)),PositiveQ(a)))),
ISetDelayed(Int(Times(Power(Plus(Times(Power(x_,C2),b_DEFAULT),a_),n_),u_),x_Symbol),
    Condition(Dist(Power(a,CN1),Subst(Int(Regularize(Times(Power(Times(a,Power(Sech(x),C2)),Plus(n,C1)),SubstFor(ArcTanh(x),u,x)),x),x),x,ArcTanh(x))),And(And(And(And(FreeQ(List(a,b),x),FunctionOfQ(ArcTanh(x),u,x)),ZeroQ(Plus(a,b))),HalfIntegerQ(n)),Less(n,CN1)))),
ISetDelayed(Int(Times(Power(Plus(Times(Power(x_,C2),b_DEFAULT),a_),n_),Power(x_,m_DEFAULT),u_),x_Symbol),
    Condition(Dist(Power(a,n),Subst(Int(Regularize(Times(Power(Tanh(x),m),Power(Cosh(x),Times(CN2,Plus(n,C1))),SubstFor(ArcTanh(x),u,x)),x),x),x,ArcTanh(x))),And(And(And(And(And(And(FreeQ(List(a,b),x),FunctionOfQ(ArcTanh(x),u,x)),ZeroQ(Plus(a,b))),HalfIntegerQ(n)),Less(n,CN1)),PositiveQ(a)),IntIntegerQ(m)))),
ISetDelayed(Int(ArcTanh(u_),x_Symbol),
    Condition(Plus(Times(x,ArcTanh(u)),Times(CN1,Int(Regularize(Times(x,D(u,x),Power(Plus(C1,Times(CN1,Power(u,C2))),CN1)),x),x))),InverseFunctionFreeQ(u,x))),
ISetDelayed(Int(Times(ArcTanh(u_),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcTanh(u),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Power(Plus(m,C1),CN1),Int(Regularize(Times(Power(x,Plus(m,C1)),D(u,x),Power(Plus(C1,Times(CN1,Power(u,C2))),CN1)),x),x)))),And(And(And(And(FreeQ(m,x),NonzeroQ(Plus(m,C1))),InverseFunctionFreeQ(u,x)),Not(FunctionOfQ(Power(x,Plus(m,C1)),u,x))),FalseQ(PowerVariableExpn(u,Plus(m,C1),x))))),
ISetDelayed(Int(Times(ArcTanh(u_),v_),x_Symbol),
    Condition(Module(List(Set(w,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(v,x)))),Condition(Plus(Times(w,ArcTanh(u)),Times(CN1,Int(Regularize(Times(w,D(u,x),Power(Plus(C1,Times(CN1,Power(u,C2))),CN1)),x),x))),InverseFunctionFreeQ(w,x))),And(And(InverseFunctionFreeQ(u,x),Not(MatchQ(v,Condition(Power(x,m_DEFAULT),FreeQ(m,x))))),FalseQ(FunctionOfLinear(Times(v,ArcTanh(u)),x))))),
ISetDelayed(Int(Times(ArcTanh(Times(b_DEFAULT,x_)),Power(Plus(Times(Power(x_,n_DEFAULT),$p(d,true)),c_),CN1)),x_Symbol),
    Condition(Plus(Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,Times(b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)),Times(CN1,Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,Times(CN1,b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)))),And(And(FreeQ(List(b,c,d),x),IntIntegerQ(n)),Not(And(Equal(n,C2),ZeroQ(Plus(Times(Power(b,C2),c),d))))))),
ISetDelayed(Int(Times(ArcTanh(Plus(Times(b_DEFAULT,x_),a_)),Power(Plus(Times(Power(x_,n_DEFAULT),$p(d,true)),c_),CN1)),x_Symbol),
    Condition(Plus(Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,a,Times(b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)),Times(CN1,Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,Times(CN1,a),Times(CN1,b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)))),And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(n)),Not(And(Equal(n,C1),ZeroQ(Plus(Times(a,d),Times(CN1,b,c)))))))),
ISetDelayed(Int(ArcCoth(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Plus(a,Times(b,x)),ArcCoth(Plus(a,Times(b,x))),Power(b,CN1)),Times(Log(Plus(C1,Times(CN1,Power(Plus(a,Times(b,x)),C2)))),Power(Times(C2,b),CN1))),FreeQ(List(a,b),x))),
ISetDelayed(Int(ArcCoth(Plus(Times(Power(x_,n_),b_DEFAULT),a_DEFAULT)),x_Symbol),
    Condition(Plus(Times(x,ArcCoth(Plus(a,Times(b,Power(x,n))))),Times(CN1,Dist(Times(b,n),Int(Times(Power(x,n),Power(Plus(C1,Times(CN1,Power(a,C2)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Power(b,C2),Power(x,Times(C2,n)))),CN1)),x)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
ISetDelayed(Int(Times(ArcCoth(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT)),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,Power(Plus(a,Times(b,Power(x,n))),CN1))),Power(x,CN1)),x)),Times(CN1,Dist(Rational(C1,C2),Int(Times(Log(Plus(C1,Times(CN1,Power(Plus(a,Times(b,Power(x,n))),CN1)))),Power(x,CN1)),x)))),FreeQ(List(a,b,n),x))),
ISetDelayed(Int(Times(ArcCoth(Plus(Times(Power(x_,n_DEFAULT),b_DEFAULT),a_DEFAULT)),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcCoth(Plus(a,Times(b,Power(x,n)))),Power(Plus(m,C1),CN1)),Times(CN1,Dist(Times(b,n,Power(Plus(m,C1),CN1)),Int(Times(Power(x,Plus(m,n)),Power(Plus(C1,Times(CN1,Power(a,C2)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Power(b,C2),Power(x,Times(C2,n)))),CN1)),x)))),And(And(And(FreeQ(List(a,b,m),x),IntIntegerQ(n)),NonzeroQ(Plus(m,C1))),NonzeroQ(Plus(m,Times(CN1,n),C1))))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),n_DEFAULT),Power(Plus(C1,Times(CN1,Power(x_,C2))),m_)),x_Symbol),
    Condition(Module(List(Set(u,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(Power(Plus(C1,Times(CN1,Power(x,C2))),m),x)))),Plus(Times(u,Power(ArcCoth(x),n)),Times(CN1,Dist(n,Int(Expand(Times(u,Power(ArcCoth(x),Plus(n,Times(CN1,C1))),Power(Plus(C1,Times(CN1,Power(x,C2))),CN1))),x))))),And(And(IntIntegerQ(List(m,n)),Less(m,CN1)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),n_DEFAULT),Power(Plus(CN1,Power(x_,C2)),m_)),x_Symbol),
    Condition(Dist(Power(CN1,m),Int(Times(Power(Plus(C1,Times(CN1,Power(x,C2))),m),Power(ArcCoth(x),n)),x)),And(And(IntIntegerQ(List(m,n)),Less(m,CN1)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),n_DEFAULT),Power(ArcTanh(x_),p_),Power(Plus(C1,Times(CN1,Power(x_,C2))),CN1)),x_Symbol),
    Condition(Plus(Times(Power(ArcCoth(x),n),Power(ArcTanh(x),Plus(p,C1)),Power(Plus(p,C1),CN1)),Times(CN1,Dist(Times(n,Power(Plus(p,C1),CN1)),Int(Times(Power(ArcCoth(x),Plus(n,Times(CN1,C1))),Power(ArcTanh(x),Plus(p,C1)),Power(Plus(C1,Times(CN1,Power(x,C2))),CN1)),x)))),And(IntIntegerQ(List(n,p)),Less(Less(C0,n),p)))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),n_DEFAULT),Power(ArcTanh(x_),p_DEFAULT),Power(Plus(C1,Times(CN1,Power(x_,C2))),m_DEFAULT)),x_Symbol),
    Condition(Module(List(Set(u,Block(List(Set($s("ShowSteps"),False),Set($s("StepCounter"),Null)),Int(Power(Plus(C1,Times(CN1,Power(x,C2))),m),x)))),Plus(Times(u,Power(ArcCoth(x),n),Power(ArcTanh(x),p)),Times(CN1,Dist(p,Int(Times(u,Power(ArcCoth(x),n),Power(ArcTanh(x),Plus(p,Times(CN1,C1))),Power(Plus(C1,Times(CN1,Power(x,C2))),CN1)),x))),Times(CN1,Dist(n,Int(Times(u,Power(ArcCoth(x),Plus(n,Times(CN1,C1))),Power(ArcTanh(x),p),Power(Plus(C1,Times(CN1,Power(x,C2))),CN1)),x))))),And(And(And(IntIntegerQ(List(m,p,n)),Less(m,CN1)),Greater(p,C1)),Greater(n,C1)))),
ISetDelayed(Int(Times(Power(ArcCoth(x_),n_DEFAULT),Power(ArcTanh(x_),p_DEFAULT),Power(Plus(CN1,Power(x_,C2)),m_DEFAULT)),x_Symbol),
    Condition(Dist(Power(CN1,m),Int(Times(Power(Plus(C1,Times(CN1,Power(x,C2))),m),Power(ArcCoth(x),n),Power(ArcTanh(x),p)),x)),And(And(IntIntegerQ(List(m,n,p)),Less(m,CN1)),Greater(n,C0)))),
ISetDelayed(Int(Times(Power(ArcCoth(Times(a_DEFAULT,x_)),n_),x_),x_Symbol),
    Condition(Plus(Times(Plus(CN1,Times(Power(a,C2),Power(x,C2))),Power(ArcCoth(Times(a,x)),n),Power(Times(C2,Power(a,C2)),CN1)),Dist(Times(n,Power(Times(C2,a),CN1)),Int(Power(ArcCoth(Times(a,x)),Plus(n,Times(CN1,C1))),x))),And(And(FreeQ(a,x),RationalQ(n)),Greater(n,C1)))),
ISetDelayed(Int(Times(Power(ArcCoth(Plus(Times(b_DEFAULT,x_),a_DEFAULT)),n_),x_),x_Symbol),
    Condition(Plus(Times(Plus(CN1,Power(Plus(a,Times(b,x)),C2)),Power(ArcCoth(Plus(a,Times(b,x))),n),Power(Times(C2,Power(b,C2)),CN1)),Dist(Times(n,Power(Times(C2,b),CN1)),Int(Power(ArcCoth(Plus(a,Times(b,x))),Plus(n,Times(CN1,C1))),x)),Times(CN1,Dist(Times(a,Power(b,CN1)),Int(Power(ArcCoth(Plus(a,Times(b,x))),n),x)))),And(And(FreeQ(List(a,b),x),RationalQ(n)),Greater(n,C1))))
  );
}
