package org.matheclipse.core.integrate.rubi;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * UtilityFunctions rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class UtilityFunctions2 { 
  public static IAST RULES = List( 
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Cot(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(u,Power(Csc(z),C2)),w),SameQ(u,v))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Sec(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(v,Power(Tan(z),C2)),w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Csc(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(v,Power(Cot(z),C2)),w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Cos(v_),b_DEFAULT),a_),CN1),Power(Sin(v_),C2),u_DEFAULT)),
    Condition(Times(u,Plus(Power(a,CN1),Times(CN1,Cos(v),Power(b,CN1)))),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2)))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cos(v_),C2),Power(Plus(Times(Sin(v_),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Plus(Power(a,CN1),Times(CN1,Sin(v),Power(b,CN1)))),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2)))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Tan(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Tan(v_),n_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Cot(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cot(v_),n_DEFAULT),Power(Plus(Times(Power(Cot(v_),n_DEFAULT),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Tan(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Sec(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Sec(v_),n_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Cos(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Csc(v_),n_DEFAULT),Power(Plus(Times(Power(Csc(v_),n_DEFAULT),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Sin(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Power(Plus(Times(Csc(v_),a_DEFAULT),Times(Cot(v_),b_DEFAULT)),n_)),
    Condition(Times(Power(a,n),Power(Cot(Times(Rational(C1,C2),v)),n)),And(EvenQ(n),ZeroQ(Plus(a,Times(CN1,b)))))),
ISetDelayed(TrigSimplifyAux(Power(Plus(Times(Csc(v_),a_DEFAULT),Times(Cot(v_),b_DEFAULT)),n_)),
    Condition(Times(Power(a,n),Power(Tan(Times(Rational(C1,C2),v)),n)),And(EvenQ(n),ZeroQ(Plus(a,b))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Cot(v_),n_DEFAULT),b_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sin(v_),m_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sin(v),Plus(m,Times(CN1,n,p))),Power(Plus(Times(b,Power(Cos(v),n)),Times(a,Power(Sin(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cos(v_),m_DEFAULT),Power(Plus(Times(Power(Tan(v_),n_DEFAULT),b_DEFAULT),a_DEFAULT),p_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Cos(v),Plus(m,Times(CN1,n,p))),Power(Plus(Times(b,Power(Sin(v),n)),Times(a,Power(Cos(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Tan(v_),C2),b_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sec(v_),m_DEFAULT),u_)),
    Condition(Times(u,Power(Plus(Times(b,Power(Sin(v),C2)),Times(a,Power(Cos(v),C2))),p)),And(IntIntegerQ(List(m,p)),Equal(Plus(m,Times(C2,p)),C0)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Csc(v_),m_DEFAULT),Power(Plus(Times(Power(Cot(v_),C2),b_DEFAULT),a_DEFAULT),p_DEFAULT),u_)),
    Condition(Times(u,Power(Plus(Times(b,Power(Cos(v),C2)),Times(a,Power(Sin(v),C2))),p)),And(IntIntegerQ(List(m,p)),Equal(Plus(m,Times(C2,p)),C0)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cos(v_),m_DEFAULT),Power(Plus(Times(Power(Tan(v_),n_DEFAULT),b_DEFAULT),Times(Power(Sec(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Cos(v),Plus(m,Times(CN1,n,p))),Power(Plus(c,Times(b,Power(Sin(v),n)),Times(a,Power(Cos(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Tan(v_),n_DEFAULT),b_DEFAULT),Times(Power(Sec(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sec(v_),m_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sec(v),Plus(m,Times(n,p))),Power(Plus(c,Times(b,Power(Sin(v),n)),Times(a,Power(Cos(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Cot(v_),n_DEFAULT),b_DEFAULT),Times(Power(Csc(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sin(v_),m_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sin(v),Plus(m,Times(CN1,n,p))),Power(Plus(c,Times(b,Power(Cos(v),n)),Times(a,Power(Sin(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Csc(v_),m_DEFAULT),Power(Plus(Times(Power(Cot(v_),n_DEFAULT),b_DEFAULT),Times(Power(Csc(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Csc(v),Plus(m,Times(n,p))),Power(Plus(c,Times(b,Power(Cos(v),n)),Times(a,Power(Sin(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Sec(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Tan(v_),n_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sin(v),n),Power(Plus(b,Times(a,Power(Cos(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cot(v_),n_DEFAULT),Power(Plus(Times(Power(Csc(v_),n_DEFAULT),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Power(Cos(v),n),Power(Plus(b,Times(a,Power(Sin(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Cos(v_),n_DEFAULT),a_DEFAULT),Times(Power(Sin(v_),n_DEFAULT),b_DEFAULT)),p_),Power(Sin(v_),m_DEFAULT))),
    Condition(Power(Plus(b,Times(a,Power(Cot(v),n))),p),And(And(And(IntIntegerQ(List(m,n,p)),Greater(n,C0)),Less(p,C0)),Equal(m,Times(CN1,n,p))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cos(v_),m_DEFAULT),Power(Plus(Times(Power(Cos(v_),n_DEFAULT),a_DEFAULT),Times(Power(Sin(v_),n_DEFAULT),b_DEFAULT)),p_))),
    Condition(Power(Plus(a,Times(b,Power(Tan(v),n))),p),And(And(And(IntIntegerQ(List(m,n,p)),Greater(n,C0)),Less(p,C0)),Equal(m,Times(CN1,n,p))))),
ISetDelayed(TrigSimplifyAux(Power(Plus(Times(Cos(v_),a_DEFAULT),Times(Sin(v_),b_DEFAULT)),n_)),
    Condition(Power(Plus(Times(Cos(v),Power(a,CN1)),Times(Sin(v),Power(b,CN1))),Times(CN1,n)),And(And(IntIntegerQ(n),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Cosh(z_),C2),u_DEFAULT),Times(Power(Sinh(z_),C2),v_DEFAULT),w_DEFAULT)),
    Condition(Plus(u,w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Sech(z_),C2),u_DEFAULT),Times(Power(Tanh(z_),C2),v_DEFAULT),w_DEFAULT)),
    Condition(Plus(u,w),SameQ(u,v))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Coth(z_),C2),u_DEFAULT),Times(Power(Csch(z_),C2),v_DEFAULT),w_DEFAULT)),
    Condition(Plus(u,w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Sinh(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(u,Power(Cosh(z),C2)),w),SameQ(u,v))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Cosh(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(v,Power(Sinh(z),C2)),w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Tanh(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(u,Power(Sech(z),C2)),w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Coth(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(v,Power(Csch(z),C2)),w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Sech(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(u,Power(Tanh(z),C2)),w),SameQ(u,Times(CN1,v)))),
ISetDelayed(TrigSimplifyAux(Plus(Times(Power(Csch(z_),C2),v_DEFAULT),u_,w_DEFAULT)),
    Condition(Plus(Times(u,Power(Coth(z),C2)),w),SameQ(u,v))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Cosh(v_),b_DEFAULT),a_),CN1),Power(Sinh(v_),C2),u_DEFAULT)),
    Condition(Times(u,Plus(Times(CN1,Power(a,CN1)),Times(Cosh(v),Power(b,CN1)))),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2)))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cosh(v_),C2),Power(Plus(Times(Sinh(v_),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Plus(Power(a,CN1),Times(Sinh(v),Power(b,CN1)))),ZeroQ(Plus(Power(a,C2),Power(b,C2))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Tanh(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Tanh(v_),n_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Coth(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Coth(v_),n_DEFAULT),Power(Plus(Times(Power(Coth(v_),n_DEFAULT),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Tanh(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Sech(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Sech(v_),n_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Cosh(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Csch(v_),n_DEFAULT),Power(Plus(Times(Power(Csch(v_),n_DEFAULT),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Power(Plus(b,Times(a,Power(Sinh(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Power(Plus(Times(Csch(v_),a_DEFAULT),Times(Coth(v_),b_DEFAULT)),n_)),
    Condition(Times(Power(a,n),Power(Coth(Times(Rational(C1,C2),v)),n)),And(EvenQ(n),ZeroQ(Plus(a,Times(CN1,b)))))),
ISetDelayed(TrigSimplifyAux(Power(Plus(Times(Csch(v_),a_DEFAULT),Times(Coth(v_),b_DEFAULT)),n_)),
    Condition(Times(Power(b,n),Power(Tanh(Times(Rational(C1,C2),v)),n)),And(EvenQ(n),ZeroQ(Plus(a,b))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Coth(v_),n_DEFAULT),b_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sinh(v_),m_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sinh(v),Plus(m,Times(CN1,n,p))),Power(Plus(Times(b,Power(Cosh(v),n)),Times(a,Power(Sinh(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cosh(v_),m_DEFAULT),Power(Plus(Times(Power(Tanh(v_),n_DEFAULT),b_DEFAULT),a_DEFAULT),p_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Cosh(v),Plus(m,Times(CN1,n,p))),Power(Plus(Times(b,Power(Sinh(v),n)),Times(a,Power(Cosh(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Tanh(v_),C2),b_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sech(v_),m_DEFAULT),u_)),
    Condition(Times(u,Power(Plus(Times(b,Power(Sinh(v),C2)),Times(a,Power(Cosh(v),C2))),p)),And(IntIntegerQ(List(m,p)),Equal(Plus(m,Times(C2,p)),C0)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Csch(v_),m_DEFAULT),Power(Plus(Times(Power(Coth(v_),C2),b_DEFAULT),a_DEFAULT),p_DEFAULT),u_)),
    Condition(Times(u,Power(Plus(Times(b,Power(Cosh(v),C2)),Times(a,Power(Sinh(v),C2))),p)),And(IntIntegerQ(List(m,p)),Equal(Plus(m,Times(C2,p)),C0)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cosh(v_),m_DEFAULT),Power(Plus(Times(Power(Tanh(v_),n_DEFAULT),b_DEFAULT),Times(Power(Sech(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Cosh(v),Plus(m,Times(CN1,n,p))),Power(Plus(c,Times(b,Power(Sinh(v),n)),Times(a,Power(Cosh(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Tanh(v_),n_DEFAULT),b_DEFAULT),Times(Power(Sech(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sech(v_),m_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sech(v),Plus(m,Times(n,p))),Power(Plus(c,Times(b,Power(Sinh(v),n)),Times(a,Power(Cosh(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Coth(v_),n_DEFAULT),b_DEFAULT),Times(Power(Csch(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),Power(Sinh(v_),m_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sinh(v),Plus(m,Times(CN1,n,p))),Power(Plus(c,Times(b,Power(Cosh(v),n)),Times(a,Power(Sinh(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Csch(v_),m_DEFAULT),Power(Plus(Times(Power(Coth(v_),n_DEFAULT),b_DEFAULT),Times(Power(Csch(v_),n_DEFAULT),c_DEFAULT),a_DEFAULT),p_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Csch(v),Plus(m,Times(n,p))),Power(Plus(c,Times(b,Power(Cosh(v),n)),Times(a,Power(Sinh(v),n))),p)),IntIntegerQ(List(m,n,p)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Sech(v_),n_DEFAULT),b_DEFAULT),a_),CN1),Power(Tanh(v_),n_DEFAULT),u_DEFAULT)),
    Condition(Times(u,Power(Sinh(v),n),Power(Plus(b,Times(a,Power(Cosh(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Coth(v_),n_DEFAULT),Power(Plus(Times(Power(Csch(v_),n_DEFAULT),b_DEFAULT),a_),CN1),u_DEFAULT)),
    Condition(Times(u,Power(Cosh(v),n),Power(Plus(b,Times(a,Power(Sinh(v),n))),CN1)),And(And(IntIntegerQ(n),Greater(n,C0)),NonsumQ(a)))),
ISetDelayed(TrigSimplifyAux(Times(Power(Plus(Times(Power(Cosh(v_),n_DEFAULT),a_DEFAULT),Times(Power(Sinh(v_),n_DEFAULT),b_DEFAULT)),p_),Power(Sinh(v_),m_DEFAULT))),
    Condition(Power(Plus(b,Times(a,Power(Coth(v),n))),p),And(And(And(IntIntegerQ(List(m,n,p)),Greater(n,C0)),Less(p,C0)),Equal(m,Times(CN1,n,p))))),
ISetDelayed(TrigSimplifyAux(Times(Power(Cosh(v_),m_DEFAULT),Power(Plus(Times(Power(Cosh(v_),n_DEFAULT),a_DEFAULT),Times(Power(Sinh(v_),n_DEFAULT),b_DEFAULT)),p_))),
    Condition(Power(Plus(a,Times(b,Power(Tanh(v),n))),p),And(And(And(IntIntegerQ(List(m,n,p)),Greater(n,C0)),Less(p,C0)),Equal(m,Times(CN1,n,p))))),
ISetDelayed(TrigSimplifyAux(Power(Plus(Times(Cosh(v_),a_DEFAULT),Times(Sinh(v_),b_DEFAULT)),n_)),
    Condition(Power(Plus(Times(Cosh(v),Power(a,CN1)),Times(CN1,Sinh(v),Power(b,CN1))),Times(CN1,n)),And(And(IntIntegerQ(n),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(TrigSimplifyAux(u_),
    u),
ISetDelayed(SmartTrigExpand(Times(Sec(u_),Sin(Times(n_,u_))),x_Symbol),
    Condition(Sum(Times(Power(CN1,Plus(k,Times(Rational(C1,C2),n))),C2,Sin(Times(Plus(Times(C2,k),Times(CN1,C1)),u))),List(k,C1,Times(Rational(C1,C2),n))),And(And(EvenQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Sec(u_),Sin(Times(n_,u_))),x_Symbol),
    Condition(Plus(Times(Power(CN1,Times(Rational(C1,C2),Plus(n,Times(CN1,C1)))),Tan(u)),Sum(Times(Power(CN1,Plus(k,Times(Rational(C1,C2),Plus(n,Times(CN1,C1))))),C2,Sin(Times(C2,k,u))),List(k,C1,Times(Rational(C1,C2),Plus(n,Times(CN1,C1)))))),And(And(OddQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cos(Times(n_,u_)),Csc(u_)),x_Symbol),
    Condition(Plus(Csc(u),Times(CN1,Sum(Times(C2,Sin(Times(Plus(Times(C2,k),Times(CN1,C1)),u))),List(k,C1,Times(Rational(C1,C2),n))))),And(And(EvenQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cos(Times(n_,u_)),Csc(u_)),x_Symbol),
    Condition(Plus(Cot(u),Times(CN1,Sum(Times(C2,Sin(Times(C2,k,u))),List(k,C1,Times(Rational(C1,C2),Plus(n,Times(CN1,C1))))))),And(And(OddQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Sech(u_),Sinh(Times(n_,u_))),x_Symbol),
    Condition(Sum(Times(Power(CN1,Plus(k,Times(Rational(C1,C2),n))),C2,Sinh(Times(Plus(Times(C2,k),Times(CN1,C1)),u))),List(k,C1,Times(Rational(C1,C2),n))),And(And(EvenQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Sech(u_),Sinh(Times(n_,u_))),x_Symbol),
    Condition(Plus(Times(Power(CN1,Times(Rational(C1,C2),Plus(n,Times(CN1,C1)))),Tanh(u)),Sum(Times(Power(CN1,Plus(k,Times(Rational(C1,C2),Plus(n,Times(CN1,C1))))),C2,Sinh(Times(C2,k,u))),List(k,C1,Times(Rational(C1,C2),Plus(n,Times(CN1,C1)))))),And(And(OddQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cosh(Times(n_,u_)),Csch(u_)),x_Symbol),
    Condition(Plus(Csch(u),Sum(Times(C2,Sinh(Times(Plus(Times(C2,k),Times(CN1,C1)),u))),List(k,C1,Times(Rational(C1,C2),n)))),And(And(EvenQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cosh(Times(n_,u_)),Csch(u_)),x_Symbol),
    Condition(Plus(Coth(u),Sum(Times(C2,Sinh(Times(C2,k,u))),List(k,C1,Times(Rational(C1,C2),Plus(n,Times(CN1,C1)))))),And(And(OddQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cosh(Times(n_,u_)),Power(Csch(u_),C2)),x_Symbol),
    Condition(Plus(n,Power(Csch(u),C2),Sum(Times(C4,Plus(Times(Rational(C1,C2),n),Times(CN1,k)),Cosh(Times(C2,k,u))),List(k,C1,Times(Rational(C1,C2),n)))),And(And(EvenQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cosh(Times(n_,u_)),Power(Csch(u_),C2)),x_Symbol),
    Condition(Plus(Times(Coth(u),Csch(u)),Sum(Times(C4,Plus(Times(Rational(C1,C2),Plus(n,C1)),Times(CN1,k)),Cosh(Times(Plus(Times(C2,k),Times(CN1,C1)),u))),List(k,C1,Times(Rational(C1,C2),Plus(n,C1))))),And(And(OddQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Times(Cosh(Times(n_,u_)),Power(Csch(u_),C3)),x_Symbol),
    Condition(Plus(Times(Plus(Power(n,C2),Times(CN1,C1)),Rational(C1,C2),Coth(u)),Times(Coth(u),Power(Csch(u),C2)),Sum(Times(ZZ(8L),Binomial(Plus(Times(Rational(C1,C2),Plus(n,C1)),Times(CN1,k)),C2),Sinh(Times(C2,k,u))),List(k,C1,Times(Rational(C1,C2),Plus(n,Times(CN1,C3)))))),And(And(OddQ(n),Greater(n,C1)),Not(FreeQ(u,x))))),
ISetDelayed(SmartTrigExpand(Power(u_,n_),x_Symbol),
    Condition(Module(List(Set($s("tmp"),SmartTrigExpand(u,x))),Condition(Expand(Power($s("tmp"),n),x),SumQ($s("tmp")))),And(And(NonsumQ(u),IntIntegerQ(n)),Greater(n,C1)))),
ISetDelayed(SmartTrigExpand(Times(u_,v_),x_Symbol),
    Condition(Module(List(Set($s("tmp1"),SmartTrigExpand(u,x)),Set($s("tmp2"),SmartTrigExpand(v,x))),Condition(Distribute(Times($s("tmp1"),$s("tmp2"))),Or(SumQ($s("tmp1")),SumQ($s("tmp2"))))),And(NonsumQ(u),NonsumQ(v)))),
ISetDelayed(SmartTrigExpand(u_,x_Symbol),
    u),
ISetDelayed(Simp($p("expn")),
    If(AtomQ($s("expn")),$s("expn"),If(Or(Or(SameQ(Head($s("expn")),$s("If")),SameQ(Head($s("expn")),$s("Int"))),SameQ(Head($s("expn")),$s("Dif"))),$s("expn"),SimpAux(Map($s("Integrate::Simp"),$s("expn")))))),
ISetDelayed(SimpAux($p("expn")),
    Condition(Module(List(Set($s("tmp"),ContentFactor($s("expn"))),$s("lst"),Set(i,C1),j),If(UnsameQ($s("tmp"),$s("expn")),CompoundExpression(Set($s("tmp"),Simp($s("tmp"))),If(ProductQ($s("tmp")),ContentFactor($s("tmp")),$s("tmp"))),CompoundExpression(CompoundExpression(CompoundExpression(CompoundExpression(CompoundExpression(Set($s("lst"),Apply($s("List"),$s("expn"))),If(And(And(And(And(And(And(And(And(GreaterEqual(Length($s("lst")),C4),RationalQ(Part($s("lst"),C1))),PowerQ(Part($s("lst"),C2))),RationalQ(Part($s("lst"),C2,C1))),RationalQ(Part($s("lst"),C2,C2))),SqrtNumberSumQ(Part($s("lst"),C3))),PowerQ(Part($s("lst"),C4))),SqrtNumberSumQ(Part($s("lst"),C4,C1))),RationalQ(Part($s("lst"),C4,C2))),Set($s("lst"),Join(List(Part($s("lst"),C3),Part($s("lst"),C4),Part($s("lst"),C1),Part($s("lst"),C2)),Drop($s("lst"),C4))))),If(And(And(And(And(And(And(And(GreaterEqual(Length($s("lst")),C3),PowerQ(Part($s("lst"),C1))),RationalQ(Part($s("lst"),C1,C1))),RationalQ(Part($s("lst"),C1,C2))),SqrtNumberSumQ(Part($s("lst"),C2))),PowerQ(Part($s("lst"),C3))),SqrtNumberSumQ(Part($s("lst"),C3,C1))),RationalQ(Part($s("lst"),C3,C2))),Set($s("lst"),Join(List(Part($s("lst"),C2),Part($s("lst"),C3),Part($s("lst"),C1)),Drop($s("lst"),C3))))),While(True,If(Equal(i,Length($s("lst"))),Break(),CompoundExpression(Set(j,Plus(i,C1)),While(True,If(Greater(j,Length($s("lst"))),CompoundExpression(Increment(i),Break()),CompoundExpression(Set($s("tmp"),SimpProduct(Part($s("lst"),i),Part($s("lst"),j))),If(UnsameQ($s("tmp"),Times(Part($s("lst"),i),Part($s("lst"),j))),If(ProductQ($s("tmp")),CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),First($s("tmp"))),Set(Part($s("lst"),j),Rest($s("tmp")))),Break()),CompoundExpression(CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),$s("tmp")),Set($s("lst"),Delete($s("lst"),j))),Set(i,C1)),Break())),CompoundExpression(Set($s("tmp"),SimpProduct(Part($s("lst"),j),Part($s("lst"),i))),If(UnsameQ($s("tmp"),Times(Part($s("lst"),j),Part($s("lst"),i))),If(ProductQ($s("tmp")),CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),First($s("tmp"))),Set(Part($s("lst"),j),Rest($s("tmp")))),Break()),CompoundExpression(CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),$s("tmp")),Set($s("lst"),Delete($s("lst"),j))),Set(i,C1)),Break())),Increment(j))))))))))),Set($s("tmp"),Apply(Times,$s("lst")))),If(ProductQ($s("tmp")),ContentFactor($s("tmp")),$s("tmp"))))),ProductQ($s("expn")))),
ISetDelayed(SimpAux($p("expn")),
    Condition(Module(List(Set($s("lst"),Apply($s("List"),$s("expn"))),Set(i,C1),j,$s("tmp")),CompoundExpression(While(True,If(Equal(i,Length($s("lst"))),Break(),CompoundExpression(Set(j,Plus(i,C1)),While(True,If(Greater(j,Length($s("lst"))),CompoundExpression(Increment(i),Break()),CompoundExpression(Set($s("tmp"),SimpSum(Part($s("lst"),i),Part($s("lst"),j))),If(UnsameQ($s("tmp"),Plus(Part($s("lst"),i),Part($s("lst"),j))),If(SumQ($s("tmp")),CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),First($s("tmp"))),Set(Part($s("lst"),j),Rest($s("tmp")))),Break()),CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),$s("tmp")),Set($s("lst"),Delete($s("lst"),j))),Break())),CompoundExpression(Set($s("tmp"),SimpSum(Part($s("lst"),j),Part($s("lst"),i))),If(UnsameQ($s("tmp"),Plus(Part($s("lst"),j),Part($s("lst"),i))),If(SumQ($s("tmp")),CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),First($s("tmp"))),Set(Part($s("lst"),j),Rest($s("tmp")))),Break()),CompoundExpression(CompoundExpression(Set(Part($s("lst"),i),$s("tmp")),Set($s("lst"),Delete($s("lst"),j))),Break())),Increment(j)))))))))),Apply(Plus,$s("lst")))),SumQ($s("expn")))),
ISetDelayed(SimpAux(Power(Plus(a_,b_),m_)),
    Condition(SimpAux(Power(Plus(Times(a,Power(Plus(Power(a,C2),Times(CN1,Power(b,C2))),CN1)),Times(CN1,b,Power(Plus(Power(a,C2),Times(CN1,Power(b,C2))),CN1))),Times(CN1,m))),And(And(And(IntIntegerQ(m),Less(m,C0)),SqrtNumberQ(a)),SqrtNumberQ(b)))),
ISetDelayed(SimpAux(Power(u_,m_)),
    Condition(Expand(Power(u,m)),And(And(IntIntegerQ(m),Greater(m,C0)),SqrtNumberSumQ(u)))),
ISetDelayed(SimpAux(Power(u_,m_)),
    Condition(Times(Power(CN1,m),SimpAux(Power(Times(CN1,u),m))),And(And(FractionQ(m),SqrtNumberSumQ(u)),NegativeQ(u)))),
ISetDelayed(SimpAux(Power(u_,m_)),
    Condition(SimpAux(Power(Expand(Power(u,Denominator(Power(m,CN1)))),Power(Numerator(Power(m,CN1)),CN1))),And(And(And(FractionQ(m),Greater(Denominator(Power(m,CN1)),C1)),SqrtNumberSumQ(u)),PositiveQ(u)))),
ISetDelayed(SimpAux(Power(Plus(a_,b_),m_)),
    Condition(Module(List(Set($s("tmp"),CommonNumericFactors(List(a,b)))),CompoundExpression(Set($s("tmp"),Abs(NumericFactor(Power(Part($s("tmp"),C1),Power(Denominator(m),CN1))))),Condition(Times(Power($s("tmp"),Numerator(m)),SimpAux(Power(Plus(Times(a,Power(Power($s("tmp"),Denominator(m)),CN1)),Times(b,Power(Power($s("tmp"),Denominator(m)),CN1))),m))),Unequal($s("tmp"),C1)))),And(And(And(FractionQ(m),Equal(Denominator(Power(m,CN1)),C1)),SqrtNumberQ(a)),SqrtNumberQ(b)))),
ISetDelayed(SimpAux(Power(Plus(a_,b_),m_)),
    Condition(Module(List(Set($s("tmp1"),Abs(Numerator(NumericFactor(Power(Plus(Power(a,C2),Times(CN1,Power(b,C2))),Power(Denominator(m),CN1)))))),$s("tmp2")),CompoundExpression(Set($s("tmp2"),Times(Plus(Power(a,C2),Times(CN1,Power(b,C2))),Power(Power($s("tmp1"),Denominator(m)),CN1))),Condition(Times(Power($s("tmp1"),Numerator(m)),SimpAux(Power(Plus(Times(a,Power($s("tmp2"),CN1)),Times(CN1,b,Power($s("tmp2"),CN1))),Times(CN1,m)))),And(And(IntIntegerQ(Times(Power(a,C2),Power(Power($s("tmp2"),C2),CN1))),IntIntegerQ(Times(Power(b,C2),Power(Power($s("tmp2"),C2),CN1)))),Or(And(Equal(Power($s("tmp2"),C2),C1),Less(m,C0)),Greater(Power($s("tmp2"),C2),C1)))))),And(And(And(And(And(And(FractionQ(m),Equal(Denominator(Power(m,CN1)),C1)),SqrtNumberQ(a)),SqrtNumberQ(b)),PositiveQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))),IntIntegerQ(Power(a,C2))),IntIntegerQ(Power(b,C2))))),
ISetDelayed(SimpAux(Power(Times(Plus(a_,b_),c_),m_)),
    Condition(SimpAux(Power(Plus(Times(c,a),Times(c,b)),m)),And(And(And(FractionQ(m),SqrtNumberQ(a)),SqrtNumberQ(b)),SqrtNumberQ(c)))),
ISetDelayed(SimpAux(Power(Plus(a_,b_),m_)),
    Condition(Module(List(Set(q,Sqrt(Plus(Power(a,C2),Times(CN1,Power(b,C2)))))),Condition(SimpAux(Power(Plus(Sqrt(Times(Rational(C1,C2),Plus(a,q))),Sqrt(Times(Rational(C1,C2),Plus(a,Times(CN1,q))))),Times(C2,m))),RationalQ(q))),And(And(And(And(And(EvenQ(Power(m,CN1)),RationalQ(a)),Greater(a,C0)),SqrtNumberQ(b)),PositiveQ(b)),PositiveQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(SimpAux(Power(Plus(a_,b_),m_)),
    Condition(Module(List(Set(q,Sqrt(Plus(Power(a,C2),Times(CN1,Power(b,C2)))))),Condition(SimpAux(Power(Plus(Sqrt(Times(Rational(C1,C2),Plus(a,q))),Times(CN1,Sqrt(Times(Rational(C1,C2),Plus(a,Times(CN1,q)))))),Times(C2,m))),RationalQ(q))),And(And(And(And(And(EvenQ(Power(m,CN1)),RationalQ(a)),Greater(a,C0)),SqrtNumberQ(b)),NegativeQ(b)),PositiveQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
ISetDelayed(SimpProduct(c_,Plus(a_,b_)),
    Condition(Plus(Times(c,a),Times(c,b)),And(And(SqrtNumberQ(a),SqrtNumberQ(b)),SqrtNumberQ(c)))),
ISetDelayed(SimpProduct(c_,Power(u_,m_)),
    Condition(Times(CN1,SimpProduct(Times(CN1,c),Power(u,m))),And(And(And(RationalQ(m),SqrtNumberSumQ(u)),SqrtNumberQ(c)),NegativeQ(c)))),
ISetDelayed(SimpProduct(Power(u_,m_),Power(v_,n_)),
    Condition(Times(Power(CN1,m),SimpProduct(Power(Times(CN1,u),m),Power(v,n))),And(And(And(RationalQ(List(m,n)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),NegativeQ(u)))),
ISetDelayed(SimpProduct(c_,Power(Plus(a_,b_),m_)),
    Condition(SimpAux(Power(Plus(Times(a,Power(c,Power(m,CN1))),Times(b,Power(c,Power(m,CN1)))),m)),And(And(And(And(And(RationalQ(m),SqrtNumberQ(a)),SqrtNumberQ(b)),SqrtNumberQ(Power(c,Power(m,CN1)))),PositiveQ(c)),Not(RationalQ(c))))),
ISetDelayed(SimpProduct(Power(u_,m_),v_),
    Condition(Module(List(Set($s("gcd"),GCD(m,C1)),w),CompoundExpression(Set(w,Expand(Times(Power(u,Times(m,Power($s("gcd"),CN1))),Power(Times(CN1,v),Power($s("gcd"),CN1))))),Condition(Times(CN1,SimpAux(Power(w,$s("gcd")))),Or(SqrtNumberQ(w),SqrtNumberSumQ(NonnumericFactors(w)))))),And(And(And(And(And(RationalQ(m),Greater(m,C0)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),PositiveQ(u)),NegativeQ(v)))),
ISetDelayed(SimpProduct(Power(u_,m_),v_),
    Condition(Module(List(Set($s("gcd"),GCD(m,C1)),w),CompoundExpression(Set(w,Simp(Times(Power(u,Times(m,Power($s("gcd"),CN1))),Power(Times(CN1,v),Power($s("gcd"),CN1))))),Condition(Times(CN1,SimpAux(Power(w,$s("gcd")))),Or(SqrtNumberQ(w),SqrtNumberSumQ(NonnumericFactors(w)))))),And(And(And(And(And(RationalQ(m),Less(m,C0)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),PositiveQ(u)),NegativeQ(v))))
  );
}
